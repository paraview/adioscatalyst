# Updating CI images

CI images are stored within this directory. Each subdirectory is a
different image.

## Tagging scheme

CI images should be tagged as:

    kitware/adioscatalyst:ci-DIRECTORY-YYYYMMDD

where `DIRECTORY` is the name of the directory and `YYYYMMDD` is the date the
image is built.

## Updating CI

The image tag needs updated in `.gitlab-ci.yml`.
