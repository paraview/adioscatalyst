#!/bin/sh

set -e

readonly adios_repo="https://github.com/ornladios/ADIOS2.git"
readonly adios_commit="v2.8.3"

readonly adios_root="/opt/adios"
readonly adios_src="$adios_root/src"
readonly adios_build="$adios_root/build"
readonly adios_install="$adios_root/install"

readonly mpich_path="/usr/lib64/mpich"

git clone -b "$adios_commit" "$adios_repo" "$adios_src"

export PATH=/opt/cmake/bin:${PATH}

cmake -GNinja \
  -S "$adios_src" \
  -B "$adios_build" \
  -DBUILD_SHARED_LIBS=ON \
  -DADIOS2_BUILD_EXAMPLES=OFF \
  -DBUILD_TESTING=OFF \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX="$adios_install" \
  -DCMAKE_PREFIX_PATH="$mpich_path" \
  -DADIOS2_USE_HDF5=OFF \
  -DADIOS2_USE_ZeroMQ=OFF \
  -DADIOS2_USE_Python=OFF \
  -DADIOS2_USE_Fortran=OFF \
  -DADIOS2_USE_SST=ON \
  -DADIOS2_USE_BZip2=OFF \
  -DADIOS2_USE_ZFP=OFF \
  -DADIOS2_USE_SZ=OFF \
  -DADIOS2_USE_MGARD=OFF \
  -DADIOS2_USE_PNG=OFF \
  -DADIOS2_USE_Blosc=OFF \
  -DADIOS2_USE_Endian_Reverse=OFF \
  -DADIOS2_USE_IME=OFF \
  -DADIOS2_USE_MPI=ON \
  -DCMAKE_INSTALL_LIBDIR=lib

cmake --build "$adios_build"
cmake --build "$adios_build" --target install

rm -rf "$adios_src" "$adios_build"
