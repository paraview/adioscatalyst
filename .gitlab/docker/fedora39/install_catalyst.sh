#!/bin/sh
set -x

readonly catalyst_repo="https://gitlab.kitware.com/paraview/catalyst.git"
readonly catalyst_commit="7358d3bb0ac2ae99b042400fb0ae0b0e93b9abca"

readonly catalyst_root="/opt/catalyst"
readonly catalyst_src="$catalyst_root/src"
readonly catalyst_build="$catalyst_root/build"
readonly catalyst_install="$catalyst_root/install"

readonly mpich_path="/usr/lib64/mpich"

git clone "$catalyst_repo" "$catalyst_src"
git -C "$catalyst_src" checkout "$catalyst_commit"

export PATH=/opt/cmake/bin:${PATH}

cmake -GNinja \
  -S "$catalyst_src" \
  -B "$catalyst_build" \
  -DCATALYST_BUILD_SHARED_LIBS=ON \
  -DCATALYST_BUILD_TESTING=OFF \
  -DCMAKE_BUILD_TYPE=Release \
  -DCATALYST_WRAP_PYTHON=ON \
  -DCATALYST_WRAP_FORTRAN=OFF \
  -DCMAKE_INSTALL_PREFIX="$catalyst_install" \
  -DCMAKE_PREFIX_PATH="$mpich_path" \
  -DCATALYST_USE_MPI=ON \
  -DCMAKE_INSTALL_LIBDIR=lib

cmake --build "$catalyst_build"
cmake --build "$catalyst_build" --target install

rm -rf "$catalyst_src" "$catalyst_build"
