#!/bin/sh

set -e

readonly cmake_root="/opt/"

mkdir -p "$cmake_root"
cd "$cmake_root"

readonly version="3.26.0"
readonly shatool="sha256sum"
readonly sha256sum="69b55523145b2e619f637e7766c413cb1b7de1f06269ea1eab4a655d59847d59"
readonly platform="linux-x86_64"

readonly filename="cmake-$version-$platform"
readonly tarball="$filename.tar.gz"

echo "$sha256sum  $tarball" > cmake.sha256sum
curl -OL "https://github.com/Kitware/CMake/releases/download/v$version/$tarball"
$shatool --check cmake.sha256sum
tar xf "$tarball"
mv "$filename" cmake

rm -rf "$tarball" cmake.sha256sum
