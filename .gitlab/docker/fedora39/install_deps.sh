#!/bin/sh

set -e

dnf install -y --setopt=install_weak_deps=False \
  glx-utils mesa-dri-drivers mesa-libGL* libxcrypt-compat.x86_64 libxkbcommon \
  libxkbcommon-x11

# Install extra dependencies for ParaView
dnf install -y --setopt=install_weak_deps=False \
  bzip2 patch git-core git-lfs

# MPI dependencies
dnf install -y --setopt=install_weak_deps=False \
  openmpi-devel mpich-devel

# Development tools
dnf install -y --setopt=install_weak_deps=False \
  libasan libtsan libubsan gcc gcc-c++ \
  gcc-gfortran ninja-build

# External dependencies
dnf install -y --setopt=install_weak_deps=False \
  libXcursor-devel libharu-devel utf8cpp-devel pugixml-devel libtiff-devel \
  eigen3-devel double-conversion-devel lz4-devel expat-devel glew-devel \
  hdf5-devel hdf5-mpich-devel hdf5-devel netcdf-devel netcdf-mpich-devel \
  libogg-devel libtheora-devel jsoncpp-devel gl2ps-devel protobuf-devel \
  boost-devel gdal-devel PDAL-devel cgnslib-devel

# Python dependencies
dnf install -y --setopt=install_weak_deps=False \
  python3-twisted python3-autobahn python3 python3-devel python3-numpy \
  python3-pandas python3-pandas-datareader python3-sphinx python3-pip \
  python3-mpi4py-mpich python3-matplotlib python-unversioned-command

python3 -m pip install cftime

dnf clean all
