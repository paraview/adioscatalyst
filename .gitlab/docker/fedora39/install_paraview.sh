#!/bin/sh

set -x

readonly paraview_repo="https://gitlab.kitware.com/paraview/paraview.git"
readonly paraview_commit="ea46d2e76bfec583cdb452dd4933340c94f1a09d"

readonly paraview_root="/opt/paraview"
readonly paraview_src="$paraview_root/src"
readonly paraview_build="$paraview_root/build"
readonly paraview_install="$paraview_root/install"

readonly mpich_path="/usr/lib64/mpich"

git clone "$paraview_repo" "$paraview_src"
git -C "$paraview_src" checkout "$paraview_commit"
git -C "$paraview_src" submodule update --init --recursive

export PATH=/opt/cmake/bin:${PATH}

cmake -GNinja \
  -S "$paraview_src" \
  -B "$paraview_build" \
  -DPARAVIEW_USE_MPI=ON \
  -DCMAKE_PREFIX_PATH="$mpich_path" \
  -DCMAKE_BUILD_TYPE=Release \
  -DPARAVIEW_ENABLE_CATALYST=ON \
  -DPARAVIEW_USE_QT=OFF \
  -DPARAVIEW_USE_PYTHON=ON \
  -Dcatalyst_DIR="/opt/catalyst/install/lib/cmake/catalyst-2.0" \
  -DCMAKE_INSTALL_PREFIX="$paraview_install"

cmake --build "$paraview_build"
cmake --build "$paraview_build" --target install

rm -rf "$paraview_src" "$paraview_build"
