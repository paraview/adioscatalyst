#include "AdiosPipeline.h"

#include "InsituAdaptor.h"
#include "catalyst_impl_adios.h"

#include <catalyst.h>
#include <catalyst_api.h>
#include <catalyst_conduit.hpp>
#include <catalyst_conduit_blueprint.hpp>
#include <catalyst_stub.h>

#include <adios2.h>
#include <iostream>

enum adios_catalyst_status
{
  adios_catalyst_status_invalid_node = 100,
  adios_catalyst_status_results = 101,
};
#define catalyst_err(name) static_cast<enum catalyst_status>(adios_catalyst_status_##name)

static AdiosPipeline* Internal = nullptr;
static InsituAdaptor* Insitu = nullptr;

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_initialize_adios(const conduit_node* params)
{
  const conduit_cpp::Node cpp_params = conduit_cpp::cpp_node(const_cast<conduit_node*>(params));

  if (!cpp_params.has_path("adios"))
  {
    std::cerr << "Initialization node does not contain 'adios' child, exiting." << std::endl;
    return catalyst_err(invalid_node);
  }

  // Parse the init node and initialize the writer configured by the adios xml config file
  auto& adios_config_filepath = cpp_params["adios"];
  if (adios_config_filepath.number_of_children() != 1)
  {
    std::cerr << "Multiple configuration files detected, only one should be specified."
              << std::endl;
    return catalyst_err(invalid_node);
  }

  auto& adios_xml = adios_config_filepath.child(0);
  std::string adios_filename;
  if (adios_xml.dtype().is_string())
  {
    adios_filename = adios_xml.as_string();
  }
  else if (adios_xml.has_child("filename"))
  {
    adios_filename = adios_xml["filename"].as_string();
  }

  if (adios_filename.find(".xml") == std::string::npos)
  {
    std::cerr << "Configuration file is not in XML format." << std::endl;
    return catalyst_err(invalid_node);
  }

  // Initialize Paraview-Catalyst implementation for hybrid in-situ mode.
  Insitu = new InsituAdaptor();
  if (cpp_params.has_path("hybrid"))
  {
    Insitu->Initialize(cpp_params["hybrid"]);
  }

  // Initialize Adios pipeline
  Internal = new AdiosPipeline();
  std::string json_node{ conduit_node_to_json(params) };
  if (!Internal->Initialize(adios_filename, json_node))
  {
    std::cerr << "Error during in-transit Adios pipeline initialization." << std::endl;
    return catalyst_err(invalid_node);
  }

  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_execute_adios(const conduit_node* params)
{
  conduit_cpp::Node cpp_params = conduit_cpp::cpp_node(const_cast<conduit_node*>(params));
  if (!cpp_params.has_path("catalyst"))
  {
    std::cerr << "Path catalyst isn't provided, skipping." << std::endl;
    return catalyst_err(invalid_node);
  }

  // Execute in-situ pipeline for hybrid mode
  if (Insitu && Insitu->IsInitialized())
  {
    Insitu->Execute(cpp_params);
    conduit_cpp::Node results;
    Insitu->Results(results);

    // Replace the original dataset produced by the simulation
    // by the steering channel, which is the output of the in-situ pipeline.
    conduit_cpp::Node resultsChannels = results["catalyst"];
    conduit_index_t nbChannels = resultsChannels.number_of_children();

    conduit_cpp::Node outputChannels;
    for (int i = 0; i < nbChannels; i++)
    {
      conduit_cpp::Node channel = resultsChannels.child(i);
      std::string channelName = channel.path();
      channelName = channelName.substr(
        channelName.find_last_of("/\\") + 1); // Extract the channel name from path
      conduit_cpp::Node currentOutputChannel = outputChannels[channelName];
      currentOutputChannel["type"].set_string("mesh");
      currentOutputChannel["data"].move(channel);
    }
    cpp_params["catalyst/channels"].move(outputChannels);
  }

  const conduit_cpp::Node root = cpp_params["catalyst"];
  const int timestep = root.has_path("state/timestep")
    ? root["state/timestep"].to_int64()
    : (root.has_path("state/cycle") ? root["state/cycle"].to_int64() : 0);
  const double time = root.has_path("state/time") ? root["state/time"].to_float64() : 0;

  // Send node through Adios
  if (Internal && !Internal->Execute(timestep, root))
  {
    std::cerr << "Error when executing the in transit pipeline" << std::endl;
    return catalyst_err(invalid_node);
  }

  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_finalize_adios(const conduit_node* params)
{
  std::string catalyst_conduit(conduit_node_to_json(params));

  if (Insitu && Insitu->IsInitialized())
  {
    conduit_cpp::Node node;
    Insitu->Finalize(node);
  }

  // Catalyst / Adios
  if (Internal && Internal->Finalize(catalyst_conduit))
  {
    delete Internal;
    Internal = nullptr;

    return catalyst_status_ok;
  }

  return catalyst_err(invalid_node);
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_about_adios(conduit_node* params)
{
  catalyst_status status = catalyst_stub_about(params);
  conduit_node_set_path_char8_str(params, "catalyst/implementation", "adios");

  return status;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_results_adios(conduit_node* params)
{
  return catalyst_stub_results(params);
}
