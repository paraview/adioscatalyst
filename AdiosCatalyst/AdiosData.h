#ifndef AdiosData_h
#define AdiosData_h

#include <adios2.h> // For adios2::Variable

#include <string>
#include <unordered_map>

/**
 * Contains all variables carefully initialized to fit with the dimension wanted.
 */
struct AdiosData
{
  // Type supported by Adios2 :
  // https://adios2.readthedocs.io/en/latest/components/components.html#data-types
  std::unordered_map<std::string, adios2::Variable<unsigned char>> VarsUChar;
  std::unordered_map<std::string, adios2::Variable<unsigned short>> VarsUShort;
  std::unordered_map<std::string, adios2::Variable<unsigned int>> VarsUInt;
  std::unordered_map<std::string, adios2::Variable<unsigned long int>> VarsULongInt;

  std::unordered_map<std::string, adios2::Variable<signed char>> VarsChar;
  std::unordered_map<std::string, adios2::Variable<short>> VarsShort;
  std::unordered_map<std::string, adios2::Variable<int>> VarsInt;
  std::unordered_map<std::string, adios2::Variable<long int>> VarsLongInt;

  std::unordered_map<std::string, adios2::Variable<float>> VarsFloat;
  std::unordered_map<std::string, adios2::Variable<double>> VarsDouble;

  std::unordered_map<std::string, adios2::Variable<std::string>> VarsString;
};
#endif
