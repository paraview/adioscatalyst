#include "AdiosPipeline.h"

#include <catalyst.h>
#include <catalyst_api.h>

#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector> 

#include <mpi.h>

namespace
{

const std::string DEFAULT_SST_FILENAME = "gs.bp";

//----------------------------------------------------------------------------
// Used to debug conduit_node, print only the conduit hierarchy name without any details.
void PrintHierarchy(const conduit_cpp::Node root, const std::string& spacing = "")
{
  conduit_index_t nbChild = root.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    const auto& child = root.child(i);
    PrintHierarchy(child, spacing + " ");
  }
}

void ExtractLeafPaths(
  const conduit_cpp::Node& entry, std::string currentPath, std::set<std::string>& paths)
{
  auto nbChildren = entry.number_of_children();
  if (nbChildren == 0)
  {
    paths.insert(currentPath);
  }

  std::string separator = (currentPath.empty() ? "" : "/");

  for (conduit_index_t iChild = 0; iChild < nbChildren; ++iChild)
  {
    ExtractLeafPaths(
      entry.child(iChild), currentPath + separator + entry.child(iChild).name(), paths);
  }
}

std::set<std::string> ExtractLeafPaths(const conduit_cpp::Node& entry, std::string currentPath)
{
  std::set<std::string> paths;
  ExtractLeafPaths(entry, currentPath, paths);
  return paths;
}

//----------------------------------------------------------------------------
// Convenient conduit method to recursively extract a specific node by name from a parent node.
bool FindNode(const conduit_cpp::Node& node, conduit_cpp::Node& output, const std::string nodeName)
{
  conduit_index_t nbChild = node.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    const conduit_cpp::Node& child = node.child(i);
    if (child.name() == nodeName)
    {
      output = child;
      return true;
    }

    if (::FindNode(child, output, nodeName))
    {
      return true;
    }
  }

  return false;
}

//----------------------------------------------------------------------------
// Return the full path of the first node named "nodeName", otherwise return an empty string
std::string GetFullPath(conduit_cpp::Node node, const std::string nodeName)
{
  std::string fullPath = "";
  const std::string separator = "/";

  conduit_index_t nbChild = node.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    conduit_cpp::Node child = node.child(i);
    std::string temp = ::GetFullPath(child, nodeName);
    temp = child.name() + separator + temp;

    if (temp.find(nodeName) != std::string::npos)
    {
      fullPath = temp;
      return fullPath;
    }
  }

  return "";
}

//----------------------------------------------------------------------------
// Convenient method to define variable
template <typename T>
void DefineVariable(std::unordered_map<std::string, adios2::Variable<T>>& container, adios2::IO io,
  std::string name, const adios2::Dims& shape, const adios2::Dims& start, const adios2::Dims& count)
{
  if (container.find(name) != container.end())
  {
    return;
  }

  adios2::Variable<T> var;
  var = io.DefineVariable<T>(name, shape, start, count);
  container.insert(std::pair<std::string, adios2::Variable<T>>(name, var));
}

//----------------------------------------------------------------------------
// Convenient method to define variable
template <typename T>
void DefineVariable(
  std::unordered_map<std::string, adios2::Variable<T>>& container, adios2::IO io, std::string name)
{
  if (container.find(name) != container.end())
  {
    return;
  }

  container.insert(std::pair<std::string, adios2::Variable<T>>(name, io.DefineVariable<T>(name)));
}
}

//----------------------------------------------------------------------------
AdiosPipeline::EntryDimensions::EntryDimensions(std::uint64_t start, std::uint64_t count, std::uint64_t shape)
{
  this->Start.emplace_back(start);
  this->Count.emplace_back(count);
  this->Shape.emplace_back(shape);
}

//----------------------------------------------------------------------------
AdiosPipeline::AdiosPipeline()
  : DimensionMap(std::make_shared<DimensionsContainer>())
  , MetaData(new AdiosData())
  , StateData(new AdiosData())
  , CoordsetsData(new AdiosData())
  , TopologicalData(new AdiosData())
  , FieldsData(new AdiosData())
{
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Initialize(
  const std::string& adiosFileName, const std::string& catalystInitializeParametersAsString)
{
  if (adiosFileName.empty())
  {
    MPI_Abort(MPI_COMM_WORLD, -1);
    std::cerr << "File path specify to locate adios xml is missing." << std::endl;
    return false;
  }

  this->AdiosFileName = adiosFileName;
  this->CatalystInitializeParametersAsString = catalystInitializeParametersAsString;
  this->FirstExecute = true;

  // Do nothing more here because we didn't have a conduit_node with variables descriptions
  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Execute(int timestep, conduit_cpp::Node root)
{
  // Because we didn't have the root node during the catalyst initialize method, we initialize all
  // variables before we treat the first execute call.
  if (this->FirstExecute)
  {
    if (!this->InitializeVariables(root))
    {
      std::cerr << "Can't initialize all variables correctly." << std::endl;
      return false;
    }

    this->FirstExecute = false;
  }

  return this->Put(root, timestep);
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Finalize(const std::string& catalystFinalizeParametersAsString)
{
  if (!catalystFinalizeParametersAsString.empty())
  {
    this->IO.DefineAttribute<std::string>(
      "CatalystFinalizeParameters", catalystFinalizeParametersAsString);
  }

  this->Writer.Close();

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::InitializeVariables(const conduit_cpp::Node& channel)
{
  try
  {
    int rank = 0;
    if(MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS)
    {
      std::cerr << "Can't get MPI rank." << std::endl;
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      return false;
    }

    int nProcs = 1;
    if(MPI_Comm_size(MPI_COMM_WORLD, &nProcs) != MPI_SUCCESS)
    {
      std::cerr << "Can't get MPI communicator size." << std::endl;
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      return false;
    }

    // Initialize Adios2
    this->Adios = adios2::ADIOS(this->AdiosFileName, MPI_COMM_WORLD);

    std::string sstFileName = ::DEFAULT_SST_FILENAME;
    if (channel.has_path("state/sstFileName"))
    {
      sstFileName = channel["state/sstFileName"].as_string();
    }

    this->IO = this->Adios.DeclareIO("Writer");
    this->Writer = this->IO.Open(sstFileName, adios2::Mode::Write, MPI_COMM_WORLD);

    if (!this->DetermineDimensions(channel))
    {
      std::cerr
        << "Was unable to successfully determine the dimensions of the entries in the channel"
        << std::endl;
      return false;
    }

    bool sucessfullyInitiliazed = this->FillStateData(channel, "catalyst");
    sucessfullyInitiliazed &= this->FillMetaData(channel, "catalyst");
    sucessfullyInitiliazed &= this->FillCoordsetsData(channel, "catalyst");
    sucessfullyInitiliazed &= this->FillTopologiesData(channel, "catalyst");

    if (!sucessfullyInitiliazed)
    {
      std::cerr << "Could not successfully initialize ADIOS channel" << std::endl;
      return false;
    }

    conduit_cpp::Node fields;
    bool finded = ::FindNode(channel, fields, "fields");
    if (finded)
    {
      conduit_cpp::Node type;
      if (!::FindNode(channel, type, "type"))
      {
        std::cerr << "Can't find node named 'type' in fields data." << std::endl;
        sucessfullyInitiliazed = false;
      }
      else
      {
        std::string fullPath = "catalyst/" + ::GetFullPath(channel, "data");
        if (type.as_string() != "multimesh")
        {
          sucessfullyInitiliazed &= this->FillFieldsData(fields, fullPath + "fields");
        }
        else
        {
          conduit_cpp::Node data;
          if (!::FindNode(channel, data, "data"))
          {
            std::cerr << "Can't find node named 'data' in fields data." << std::endl;
            sucessfullyInitiliazed = false;
          }
          else
          {
            int numberOfMesh = data.number_of_children();
            for (int i = 0; i < numberOfMesh; i++)
            {
              conduit_cpp::Node fieldsNode;
              if (!::FindNode(data.child(i), fieldsNode, "fields"))
              {
                std::cerr << "Can't find node named 'fields' in fields data for "
                          << data.child(i).name() << "." << std::endl;
              }
              else
              {
                std::string metaPath = fullPath + data.child(i).name() + "/fields";
                sucessfullyInitiliazed &= this->FillFieldsData(fieldsNode, metaPath);
              }
            }
          }
        }
      }
    }

    if (sucessfullyInitiliazed)
    {
      // Set all attributes
      this->IO.DefineAttribute<std::string>(
        "CatalystInitializeParameters", this->CatalystInitializeParametersAsString);
    }
    return sucessfullyInitiliazed;
  }
  catch (const std::exception& error)
  {
    std::cerr << error.what() << std::endl;
    return false;
  }
}

//----------------------------------------------------------------------------
bool AdiosPipeline::DetermineDimensions(const conduit_cpp::Node& entry)
{
  std::set<std::string> leaves = ::ExtractLeafPaths(entry, "");
  std::uint64_t nLeaves = leaves.size();

  // check if all ranks have the same number of leaves
  std::uint64_t nRootLeaves = nLeaves;
  if (MPI_Bcast(&nRootLeaves, 1, MPI_UINT64_T, 0, MPI_COMM_WORLD) != MPI_SUCCESS)
  {
    std::cerr << "Error broadcasting number of leaves" << std::endl;
    return false;
  }
  bool isSameNumberAsRoot = (nLeaves == nRootLeaves);
  bool allEqual = false;
  if (MPI_Allreduce(&isSameNumberAsRoot, &allEqual, 1, MPI_C_BOOL, MPI_LAND, MPI_COMM_WORLD) != MPI_SUCCESS)
  {
    std::cerr << "Error reducing boolean check on number of leaves" << std::endl;
    return false;

  }
  if (!allEqual)
  {
    std::cerr << "MPI ranks don't all have the same number of leaves in the conduit node"
              << std::endl;
    return false;
  }

  // determine sizes of all leaves
  std::vector<std::uint64_t> sizes;
  sizes.reserve(nLeaves);
  for (const auto& leaf : leaves)
  {
    // this happens for lists for example
    if (!entry.has_path(leaf))
    {
      sizes.emplace_back(0);
      continue;
    }
    sizes.emplace_back(entry[leaf].dtype().number_of_elements());
  }

  int rank = 0;
  int nProcs = 1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

  // gather sizes from all processes
  std::vector<std::uint64_t> allSizes(nProcs * nLeaves);
  if (MPI_Allgather(sizes.data(), sizes.size(), MPI_UINT64_T, allSizes.data(), sizes.size(),
    MPI_UINT64_T, MPI_COMM_WORLD) != MPI_SUCCESS)
  {
    std::cerr << "Error gathering size data" << std::endl;
    return false;
  }

  // compute the starts for the local rank
  std::vector<std::uint64_t> starts(nLeaves, 0);
  for (int iRank = 0; iRank < rank; ++iRank)
  {
    std::uint64_t offset = nLeaves * iRank;
    for (std::uint64_t iLeaf = 0; iLeaf < nLeaves; ++iLeaf)
    {
      starts[iLeaf] += allSizes[offset + iLeaf];
    }
  }

  // compute the total shapes for all ranks
  std::vector<std::uint64_t> shapes(starts.size());
  std::copy(starts.cbegin(), starts.cend(), shapes.begin());
  for (int iRank = rank; iRank < nProcs; ++iRank)
  {
    std::uint64_t offset = nLeaves * iRank;
    for (std::uint64_t iLeaf = 0; iLeaf < nLeaves; ++iLeaf)
    {
      shapes[iLeaf] += allSizes[offset + iLeaf];
    }
  }

  std::size_t counter = 0;
  for (const auto& leaf : leaves)
  {
    this->DimensionMap->emplace(
      "catalyst/" + leaf, EntryDimensions(starts[counter], sizes[counter], shapes[counter]));
    counter++;
  }

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillStateData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isStateData = name.find("state") != std::string::npos;
  if (nbChild == 0 && isStateData && this->SupportedType(root))
  {
    return this->FillVariables(this->StateData, root, name);
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillStateData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillMetaData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isMetaData =
    name.find("type") != std::string::npos && name.find("data") == std::string::npos;
  if (nbChild == 0 && isMetaData && this->SupportedType(root))
  {
    return this->FillVariables(this->MetaData, root, name);
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillMetaData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillCoordsetsData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isCoordsets = name.find("coordsets") != std::string::npos;
  if (nbChild == 0 && isCoordsets && this->SupportedType(root))
  {
    // Need explicitly the shape data
    if (name.find("values") != std::string::npos)
    {
      auto it = this->DimensionMap->find(name);
      if (it == this->DimensionMap->end())
      {
        std::cerr << "Could not find " << name << " in dimension map" << std::endl;
        return false;
      }
      const auto& rootDims = it->second;

      return this->FillVariables(
        this->CoordsetsData, root, name, rootDims.Shape, rootDims.Start, rootDims.Count);
    }
    else
    {
      return this->FillVariables(this->CoordsetsData, root, name);
    }
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillCoordsetsData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillTopologiesData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isTopologies = name.find("topologies") != std::string::npos;
  if (nbChild == 0 && isTopologies && this->SupportedType(root))
  {
    // Need explicitly the shape data
    if (name.find("connectivity") != std::string::npos)
    {
      auto it = this->DimensionMap->find(name);
      if (it == this->DimensionMap->end())
      {
        std::cerr << "Failed to find " << name << " in dimension map" << std::endl;
        return false;
      }
      const auto& rootDims = it->second;

      return this->FillVariables(
        this->TopologicalData, root, name, rootDims.Shape, rootDims.Start, rootDims.Count);
    }
    else
    {
      return this->FillVariables(this->TopologicalData, root, name);
    }
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillTopologiesData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillFieldsData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  for (conduit_index_t fieldId = 0; fieldId < nbChild; fieldId++)
  {
    const auto& field = root.child(fieldId);

    if (!field.has_child("association") || !field.has_child("topology"))
    {
      continue;
    }

    std::string association = field["association"].as_string();
    std::string topology = field["topology"].as_string();

    for (conduit_index_t fieldProperty = 0; fieldProperty < field.number_of_children();
         fieldProperty++)
    {
      const auto& property = field.child(fieldProperty);
      std::string fullName = name + "/" + field.name() + "/" + property.name();

      if (property.name().find("values") != std::string::npos)
      {
        if (property.number_of_children() != 0)
        {
          for (conduit_index_t i = 0; i < property.number_of_children(); i++)
          {
            const auto& component = property.child(i);
            auto it = this->DimensionMap->find(fullName + "/" + component.name());
            if (it == this->DimensionMap->end())
            {
              std::cerr << "Failed to find " << fullName + "/" + component.name()
                        << " in dimension map" << std::endl;
              return false;
            }
            const auto& compDims = it->second;
            isInitialized &= this->FillVariables(this->FieldsData, component,
              fullName + "/" + component.name(), compDims.Shape, compDims.Start, compDims.Count);
          }
        }
        else
        {
          auto it = this->DimensionMap->find(fullName);
          if (it == this->DimensionMap->end())
          {
            std::cerr << "Failed to find " << fullName << " in dimension map" << std::endl;
            return false;
          }
          const auto& dims = it->second;
          isInitialized &= this->FillVariables(
            this->FieldsData, property, fullName, dims.Shape, dims.Start, dims.Count);
        }
      }
      else
      {
        isInitialized &= this->FillVariables(this->FieldsData, property, fullName);
      }
    }
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::SupportedType(const conduit_cpp::Node& node)
{
  bool supported = false;
  supported |= node.dtype().name() == "char8_str";
  supported |= node.dtype().name() == "int8";
  supported |= node.dtype().name() == "int16";
  supported |= node.dtype().name() == "int32";
  supported |= node.dtype().name() == "int64";
  supported |= node.dtype().name() == "uint8";
  supported |= node.dtype().name() == "uint16";
  supported |= node.dtype().name() == "uint32";
  supported |= node.dtype().name() == "uint64";
  supported |= node.dtype().name() == "float32";
  supported |= node.dtype().name() == "float64";
  supported |= node.dtype().name() == "std::string";

  return supported;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillVariables(
  std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values, const std::string& name)
{
  auto conduitType = values.dtype().name();

  if (conduitType == "char8_str")
  {
    if (data->VarsString.find(name) != data->VarsString.end())
    {
      return true;
    }
    data->VarsString.insert(std::pair<std::string, adios2::Variable<std::string>>(
      name, this->IO.DefineVariable<std::string>(name)));
  }
  else if (conduitType == "int8")
  {
    ::DefineVariable<signed char>(data->VarsChar, this->IO, name);
  }
  else if (conduitType == "int16")
  {
    ::DefineVariable<short>(data->VarsShort, this->IO, name);
  }
  else if (conduitType == "int32")
  {
    ::DefineVariable<int>(data->VarsInt, this->IO, name);
  }
  else if (conduitType == "int64")
  {
    ::DefineVariable<long int>(data->VarsLongInt, this->IO, name);
  }
  else if (conduitType == "uint8")
  {
    ::DefineVariable<unsigned char>(data->VarsUChar, this->IO, name);
  }
  else if (conduitType == "uint16")
  {
    ::DefineVariable<unsigned short>(data->VarsUShort, this->IO, name);
  }
  else if (conduitType == "uint32")
  {
    ::DefineVariable<unsigned int>(data->VarsUInt, this->IO, name);
  }
  else if (conduitType == "uint64")
  {
    ::DefineVariable<unsigned long int>(data->VarsULongInt, this->IO, name);
  }
  else if (conduitType == "float32")
  {
    ::DefineVariable<float>(data->VarsFloat, this->IO, name);
  }
  else if (conduitType == "float64")
  {
    ::DefineVariable<double>(data->VarsDouble, this->IO, name);
  }
  else
  {
    std::cerr << "Type of " << name << " is " << conduitType << " and it's not currently supported."
              << std::endl;
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillVariables(std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values,
  const std::string& name, const adios2::Dims& shape, const adios2::Dims& start,
  const adios2::Dims& offset)
{
  auto conduitType = values.dtype().name();

  if (conduitType == "int8")
  {
    ::DefineVariable<signed char>(data->VarsChar, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "int16")
  {
    ::DefineVariable<short>(data->VarsShort, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "int32")
  {
    ::DefineVariable<int>(data->VarsInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "int64")
  {
    ::DefineVariable<long int>(data->VarsLongInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint8")
  {
    ::DefineVariable<unsigned char>(data->VarsUChar, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint16")
  {
    ::DefineVariable<unsigned short>(data->VarsUShort, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint32")
  {
    ::DefineVariable<unsigned int>(data->VarsUInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint64")
  {
    ::DefineVariable<unsigned long int>(data->VarsULongInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "float32")
  {
    ::DefineVariable<float>(data->VarsFloat, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "float64")
  {
    ::DefineVariable<double>(data->VarsDouble, this->IO, name, shape, start, offset);
  }
  else
  {
    std::cerr << "values type " << conduitType << " isn't currently supported." << std::endl;
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Put(conduit_cpp::Node root, int timestep)
{
  bool successfullyPut = true;
  conduit_cpp::Node stateNode;
  ::FindNode(root, stateNode, "state");

  this->Writer.BeginStep();

  successfullyPut &= this->PreparePutVariables(this->StateData, stateNode, "catalyst/state");

  // MetaData and mesh informations depend on the conduit protocol used in the adaptor.
  conduit_cpp::Node type;
  ::FindNode(root, type, "type");

  // retrieve non-static relative mesh path introspectively
  std::string fullPath = "catalyst/" + ::GetFullPath(root, "data");
  int fieldPos = fullPath.find("data");
  std::string commonPath = fullPath.substr(0, fieldPos);

  if (type.as_string() == "multimesh")
  {
    conduit_cpp::Node dataNode;
    ::FindNode(root, dataNode, "data");

    int numberOfMesh = dataNode.number_of_children();

    for (int i = 0; i < numberOfMesh; i++)
    {
      conduit_cpp::Node metaDataNode, coordsetsNode, topologiesNode, fieldsNode;
      ::FindNode(root, metaDataNode, "type");
      ::FindNode(dataNode.child(i), coordsetsNode, "coordsets");
      ::FindNode(dataNode.child(i), topologiesNode, "topologies");
      ::FindNode(dataNode.child(i), fieldsNode, "fields");

      std::string path = fullPath + dataNode.child(i).name();

      std::string metaPath = commonPath + "type";
      successfullyPut &= this->PreparePutVariables(this->MetaData, metaDataNode, metaPath);
      metaPath = path + "/coordsets";
      successfullyPut &= this->PreparePutVariables(this->CoordsetsData, coordsetsNode, metaPath);

      metaPath = path + "/topologies";
      successfullyPut &= this->PreparePutVariables(this->TopologicalData, topologiesNode, metaPath);
      metaPath = path + "/fields";
      successfullyPut &= this->PreparePutVariables(this->FieldsData, fieldsNode, metaPath);
    }
  }
  else
  {
    conduit_cpp::Node coordsetsNode;
    conduit_cpp::Node metaDataNode, topologiesNode, fieldsNode;
    ::FindNode(root, metaDataNode, "type");
    ::FindNode(root, coordsetsNode, "coordsets");
    ::FindNode(root, topologiesNode, "topologies");
    ::FindNode(root, fieldsNode, "fields");

    successfullyPut &= this->PreparePutVariables(this->MetaData, metaDataNode, commonPath + "type");
    successfullyPut &=
      this->PreparePutVariables(this->CoordsetsData, coordsetsNode, commonPath + "data/coordsets");
    successfullyPut &= this->PreparePutVariables(
      this->TopologicalData, topologiesNode, commonPath + "data/topologies");
    successfullyPut &=
      this->PreparePutVariables(this->FieldsData, fieldsNode, commonPath + "data/fields");
  }

  this->Writer.EndStep();

  return successfullyPut;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::PreparePutVariables(
  std::shared_ptr<AdiosData> data, const conduit_cpp::Node& root, std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();
  if (nbChild == 0 && this->SupportedType(root))
  {
    return this->PutVariables(data, root, name);
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    const auto& child = root.child(i);
    isInitialized &= this->PreparePutVariables(data, child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::PutVariables(
  std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values, const std::string& name)
{
  try
  {
    auto conduitType = values.dtype().name();
    if (conduitType == "char8_str")
    {
      auto it = data->VarsString.find(name.c_str());
      if (it == data->VarsString.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, std::string(values.as_char8_str()));
    }
    else if (conduitType == "int8")
    {
      auto it = data->VarsChar.find(name.c_str());
      if (it == data->VarsChar.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_int8_ptr());
    }
    else if (conduitType == "int16")
    {
      auto it = data->VarsShort.find(name.c_str());
      if (it == data->VarsShort.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_int16_ptr());
    }
    else if (conduitType == "int32")
    {
      auto it = data->VarsInt.find(name.c_str());
      if (it == data->VarsInt.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_int32_ptr());
    }
    else if (conduitType == "int64")
    {
      auto it = data->VarsLongInt.find(name.c_str());
      if (it == data->VarsLongInt.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_int64_ptr());
    }
    else if (conduitType == "uint8")
    {
      auto it = data->VarsUChar.find(name.c_str());
      if (it == data->VarsUChar.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_uint8_ptr());
    }
    else if (conduitType == "uint16")
    {
      auto it = data->VarsUShort.find(name.c_str());
      if (it == data->VarsUShort.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_uint16_ptr());
    }
    else if (conduitType == "uint32")
    {
      auto it = data->VarsUInt.find(name.c_str());
      if (it == data->VarsUInt.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_uint32_ptr());
    }
    else if (conduitType == "uint64")
    {
      auto it = data->VarsULongInt.find(name.c_str());
      if (it == data->VarsULongInt.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_uint64_ptr());
    }
    else if (conduitType == "float32")
    {
      auto it = data->VarsFloat.find(name.c_str());
      if (it == data->VarsFloat.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_float32_ptr());
    }
    else if (conduitType == "float64")
    {
      auto it = data->VarsDouble.find(name.c_str());
      if (it == data->VarsDouble.end())
      {
        std::cerr << "Warning: could not find " << name << " in initialized variables. Skipping it."
                  << std::endl;
        return true;
      }
      this->Writer.Put(it->second, values.as_float64_ptr());
    }
    else
    {
      throw std::runtime_error(
        std::string("value type ") + conduitType + std::string(" isn't currently supported."));
    }
  }
  catch (const std::exception& error)
  {
    std::cerr << error.what() << std::endl;
    return false;
  }

  return true;
}
