#ifndef AdiosPipeline_h
#define AdiosPipeline_h

#include "AdiosData.h"

#include <adios2.h>

#include <catalyst_conduit.hpp>

#include <memory>        // For std::shared_ptr
#include <string>        // For std::string

class AdiosReplay;

/**
 * @class AdiosPipeline
 * @brief inTransit pipeline
 *
 * AdiosPipeline is a pipeline that can be used to save out data using writers
 * supported by Adios.
 *
 * A pipeline has three stages: Initialize, Execute and Finalize.
 *
 * `Initialize` is called exactly once before the first call to `Execute`. If
 * `Initialize` returns `false`, the initialization is deemed failed and
 * pipeline is skipped for rest of the execution i.e. either Execute nor
 * Finalize will be called.
 *
 * `Execute` is called on each cycle. If the method returns false, then the
 * execution is deemed failed and the `Execute` method will not be called in
 * subsequent cycles.
 *
 * If `Initialize` succeeded, then `Finalize` is called as the end of the
 * simulation execution. `Finalize` is called even if `Execute` returned
 * failure. However, it will not be called if `Initialize` returned failure too.
 */
class AdiosPipeline
{
public:
  AdiosPipeline();
  ~AdiosPipeline() = default;

  ///@{
  /**
   * AdiosPipeline API implementaton.
   */
  bool Initialize(
    const std::string& adiosFileName, const std::string& CatalystInitializeParametersAsString);
  bool Execute(int timestep, const conduit_cpp::Node channel);
  bool Finalize(const std::string& catalystFinalizeParametersAsString);
  ///@}

protected:
  typedef adios2::uint64_t uint64_t;

  /**
   * Return the cell type defined in the node `shape`.
   *
   * Supported cell type : point, line, tri, quad, tetra, hex.
   */
  std::string GetCellType(const conduit_cpp::Node& data);

  /**
   * Setup MPI and initialize all variables at the beginning of the first execute call because we
   * need to know each variables dimensions that is only know during the Catalyst::Execute.
   */
  bool InitializeVariables(const conduit_cpp::Node& root);

  /**
   * Define variable with the adios engine and store it inside the correct map.
   */
  bool FillVariables(
    std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values, const std::string& name);
  bool FillVariables(std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values,
    const std::string& name, const adios2::Dims& shape, const adios2::Dims& start,
    const adios2::Dims& offset);

  /**
   * Start the adios writer and find for each values in the 'fields' the PutVariables() method.
   */
  bool Put(conduit_cpp::Node root, int timestep);

  /**
   * Apply the PutVariables() by the adios engine depending on their types
   */
  bool PutVariables(const conduit_cpp::Node& values, const std::string& name);
  bool PutVariables(
    std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values, const std::string& name);

  /**
   * Return true if the node is a supported type for Adios2.
   *
   * more details : https://adios2.readthedocs.io/en/latest/components/components.html#data-types
   */
  bool SupportedType(const conduit_cpp::Node& node);

  /**
   * For data recognize as optional protocol 'fields' required by the blueprint mesh, append a new
   * variable for each single child based on her type.
   */
  bool FillStateData(const conduit_cpp::Node& root, const std::string name = "");

  /**
   * For data recognize as 'coordsets' required by the blueprint mesh, append a
   * new variable for each single child based on her type.
   */
  bool FillCoordsetsData(const conduit_cpp::Node& root, const std::string name = "");

  /**
   * For data recognize as protocol 'topologies' required by the blueprint mesh, append a
   * new variable for each single child based on her type.
   */
  bool FillTopologiesData(const conduit_cpp::Node& root, const std::string name = "");

  /**
   * For data recognize as optional protocol 'fields' required by the blueprint mesh, append a new
   * variable for each single child based on her type.
   */
  bool FillFieldsData(const conduit_cpp::Node& root, const std::string name = "");

  /**
   * For all other data isn't required by the blueprint mesh, append a new variable for each single
   * child based on her type.
   */
  bool FillMetaData(const conduit_cpp::Node& root, const std::string name = "");

  /**
   * iterate on each subnode to put variables defined.
   */
  bool PreparePutVariables(const conduit_cpp::Node& root, std::string name = "");
  bool PreparePutVariables(
    std::shared_ptr<AdiosData> data, const conduit_cpp::Node& root, std::string name = "");

private:
  AdiosPipeline(const AdiosPipeline&) = delete;
  void operator=(const AdiosPipeline&) = delete;

  /**
   * A method for introspection on the conduit node and adding to the DimensionMap
   */
  bool DetermineDimensions(const conduit_cpp::Node& entry);

  // Path to a specific xml file used to setup adios engine
  std::string AdiosFileName;

  // Content of "catalyst" conduit node in a json formated string
  std::string CatalystInitializeParametersAsString = "";
  std::string CatalystRawDataAsString = "";

  adios2::ADIOS Adios;
  adios2::IO IO;
  adios2::Engine Writer;

  struct EntryDimensions
  {
    EntryDimensions(std::uint64_t start, std::uint64_t count, std::uint64_t shape);
    adios2::Dims Start;
    adios2::Dims Count;
    adios2::Dims Shape;
  };
  using DimensionsContainer = std::map<std::string, EntryDimensions>;
  std::shared_ptr<DimensionsContainer> DimensionMap;

  // Used to initialize at the first execute called, all variables
  bool FirstExecute = true;

  // Data type supported :
  // https://adios2.readthedocs.io/en/latest/components/components.html#data-types
  adios2::Variable<int> Timestep;
  std::shared_ptr<AdiosData> MetaData;
  std::shared_ptr<AdiosData> StateData;
  std::shared_ptr<AdiosData> CoordsetsData;
  std::shared_ptr<AdiosData> TopologicalData;
  std::shared_ptr<AdiosData> FieldsData;
};

#endif
