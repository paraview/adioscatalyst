#include "InsituAdaptor.h"

#include <dlfcn.h> // For dlopen and dlsym

// Internal Catalyst implementation structure of pointers to functions.
// We need to declare it in order to load the shared library,
// Because Catalyst itself does not expose it.
namespace
{
struct catalyst_impl
{
  int version;
  enum catalyst_status (*initialize)(const conduit_node*);
  enum catalyst_status (*execute)(const conduit_node*);
  enum catalyst_status (*finalize)(const conduit_node*);
  enum catalyst_status (*about)(conduit_node*);
  enum catalyst_status (*results)(conduit_node*);
};

const std::string PATH_SEPARATOR{ ":" };
}

//-----------------------------------------------------------------------------
class InsituAdaptor::ParaViewCatalystImplementation
{
public:
  ParaViewCatalystImplementation() = default;
  ~ParaViewCatalystImplementation() = default;

  /**
   * Load a Catalyst implementation from a path
   */
  int Load(const std::string&& implName)
  {
    // Retrieve and split catalyst implementation paths from environment variable
    std::vector<std::string> implPaths;
    const std::string pathEnvvar{ getenv("CATALYST_IMPLEMENTATION_PATHS") };
    split_paths(pathEnvvar, implPaths);

    // Try to find shared library in the specified paths
    void* handle;
    for (auto& path : implPaths)
    {
      std::string libPath = path + "/libcatalyst-" + implName + ".so";
      handle = dlopen(libPath.c_str(), RTLD_LAZY | RTLD_GLOBAL);
      if (handle)
      {
        break;
      }
    }
    if (!handle)
    {
      std::cerr << "Failed to open Paraview Catalyst library" << std::endl;
      return 1;
    }

    // Load symbols
    this->ActualImpl = static_cast<catalyst_impl*>(dlsym(handle, "catalyst_api_impl"));
    if (!this->ActualImpl)
    {
      std::cerr << "Implementation is not Catalyst" << std::endl;
      this->ActualImpl = nullptr;
      return 1;
    }

    // Ensure that all required API functions are provided.
    if (!this->ActualImpl->initialize || !this->ActualImpl->execute ||
      !this->ActualImpl->finalize || !this->ActualImpl->about || !this->ActualImpl->results)
    {
      std::cerr << "Incomplete implementation" << std::endl;
      this->ActualImpl = nullptr;
      return 1;
    }

    return 0;
  }

  void Initialize(const conduit_cpp::Node& node)
  {
    if (this->ActualImpl)
    {
      this->ActualImpl->initialize(conduit_cpp::c_node(&node));
    }
  }

  void Execute(const conduit_cpp::Node& node)
  {
    if (this->ActualImpl)
    {
      this->ActualImpl->execute(conduit_cpp::c_node(&node));
    }
  }

  void Results(conduit_cpp::Node& node)
  {
    if (this->ActualImpl)
    {
      this->ActualImpl->results(conduit_cpp::c_node(&node));
    }
  }

  void Finalize(conduit_cpp::Node& node)
  {
    if (this->ActualImpl)
    {
      this->ActualImpl->finalize(conduit_cpp::c_node(&node));
    }
  }

private:
  catalyst_impl* ActualImpl = nullptr;

  /**
   * Split a given path, split it in a vector of string following `PATH_SEPARATOR` separator
   */
  static void split_paths(const std::string& fullPath, std::vector<std::string>& pathList)
  {
    size_t posStart = 0;
    size_t posEnd = 0;

    while ((posEnd = fullPath.find(PATH_SEPARATOR, posStart)) != std::string::npos)
    {
      pathList.push_back(fullPath.substr(posStart, posEnd - posStart));
      posStart = posEnd + 1;
    }

    pathList.push_back(fullPath.substr(posStart));
  }
};

//-----------------------------------------------------------------------------
InsituAdaptor::InsituAdaptor()
{
  this->Implementation = new ParaViewCatalystImplementation();
}

//-----------------------------------------------------------------------------
InsituAdaptor::~InsituAdaptor()
{
  delete this->Implementation;
}

//-----------------------------------------------------------------------------
int InsituAdaptor::Initialize(const conduit_cpp::Node& hybridNode)
{
  if (!hybridNode.has_child("pipeline"))
  {
    std::cerr << "No hybrid pipeline file was found." << std::endl;
    return 1;
  }

  std::string pipelineFile = hybridNode["pipeline"].as_string();

  if (this->Implementation->Load("paraview") != 0)
  {
    std::cerr << "Error loading Paraview Catalyst implementation : could not find "
                 "libcatalyst-paraview.so in the provided path. Did you set "
                 "CATALYST_IMPLEMENTATION_PATHS environment variable correctly ?"
              << std::endl;
    return 1;
  }

  conduit_cpp::Node initNode;
  initNode["catalyst/scripts/script0"].set(pipelineFile);
  initNode["catalyst_load/implementation"] = "paraview";

  this->Implementation->Initialize(initNode);
  this->Initialized = true;

  return 0;
}

//-----------------------------------------------------------------------------
void InsituAdaptor::Execute(conduit_cpp::Node& node)
{
  this->Implementation->Execute(node);
}

//-----------------------------------------------------------------------------
void InsituAdaptor::Results(conduit_cpp::Node& resultsNode)
{
  this->Implementation->Results(resultsNode);
}

//-----------------------------------------------------------------------------
void InsituAdaptor::Finalize(conduit_cpp::Node& resultsNode)
{
  this->Implementation->Finalize(resultsNode);
}
