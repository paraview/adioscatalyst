#ifndef InsituAdaptor_h
#define InsituAdaptor_h

#include <catalyst_api.h>       // For catalyst_status
#include <catalyst_conduit.hpp> // For conduit_cpp::Node

/**
 * Adaptor used for hybrid In-situ/In-transit pipelines.
 * This adaptor loads the Paraview Catalyst implementation if it is in the PATH, executes a pipeline
 * on it, and uses the Steering mechanism to return a processed dataset to the AdiosCatalyst
 * implementation.
 */
class InsituAdaptor
{
public:
  InsituAdaptor();
  ~InsituAdaptor();

  /**
   * Initialize the Paraview Catalyst implementation, directly opening the shared library if it is
   * found in the PATH.
   *
   * Return 0 if the initialization was successful, 1 otherwise.
   */
  int Initialize(const conduit_cpp::Node& hybridNode);

  /**
   * Execute the given node on the Paraview Catalyst pipeline initialized previously.
   */
  void Execute(conduit_cpp::Node& node);

  /**
   * Returns the steering nodes after the pipeline execution
   */
  void Results(conduit_cpp::Node& resultsNode);

  /**
   * Finalize the pipeline execution
   */
  void Finalize(conduit_cpp::Node& resultsNode);

  /**
   * Return true if the pipeline has been initialized correctly.
   */
  bool IsInitialized() { return this->Initialized; }

private:
  bool Initialized = false;
  class ParaViewCatalystImplementation;
  ParaViewCatalystImplementation* Implementation;
};

#endif
