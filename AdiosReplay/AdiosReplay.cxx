#include "SimulationSynchronizer.h"

#include <catalyst.hpp>

#include <adios2.h>

#include <mpi.h>

#include <iostream>
#include <string>
#include <cassert>

namespace
{
//----------------------------------------------------------------------------
bool RetrieveFilesAsArgs(int argc, char* argv[], std::string& xmlConfigFile,
  std::vector<std::string>& sstFiles, std::vector<std::string>& pipelineScripts)
{
  std::string helperMessage =
    "AdiosReplay usage : ./AdiosReplay <adios-config-file> [<sst-file1>, <sst-file2>, ...]";
  if (argc < 2)
  {
    std::cerr << "You need to specify an adios config file written in xml." << std::endl;
    std::cout << helperMessage << std::endl;
    return false;
  }

  xmlConfigFile = argv[1];
  if (xmlConfigFile.find(".xml") == std::string::npos)
  {
    std::cerr << "Specify the xml config file for adios as first parameter in the command line, "
                 "actually we got : "
              << xmlConfigFile << std::endl;
    std::cout << helperMessage << std::endl;
    return true;
  }

  if (argc < 3)
  {
    std::cout << "No adios connection file has been linked, use the default 'gs.bp.sst'."
              << std::endl;
    return true;
  }

  for (std::size_t start = 2; start < argc; start++)
  {
    std::string filepath = argv[start];
    if (filepath.find(".py") != std::string::npos)
    {
      pipelineScripts.push_back(filepath);
    }
    else
    {
      sstFiles.push_back(argv[start]);
    }
  }

  return true;
}
}

//----------------------------------------------------------------------------
// We just want to extract the conduit node stored inside the attribute and execute on it the
// catalyst implementation
catalyst_status replay_adios_initialize(
  SimulationSynchronizer& synchronizer, const std::vector<std::string>& pipelineScripts)
{
  adios2::IO io = synchronizer.GetIO();
  std::string catalystNodeVariableName = "CatalystInitializeParameters";
  auto catalystAttribute = io.InquireAttribute<std::string>("CatalystInitializeParameters");

  if (catalystAttribute)
  {
    std::string catalystNodeJson = catalystAttribute.Data()[0];
    if (!catalystNodeJson.empty())
    {
      conduit_node* catalystNode = conduit_node_create();
      conduit_node_generate(catalystNode, catalystNodeJson.c_str(), "json", nullptr);

      if (catalystNode)
      {
        // If any script is provided directly to AdiosReplay, discard all scripts provided by the
        // simulation.
        std::string scriptsPath = "catalyst/scripts";
        if (pipelineScripts.size() > 0 &&
          conduit_node_has_path(catalystNode, scriptsPath.c_str()))
        {
          std::cout << "Warning : a pipeline Python script was provided to AdiosReplay, discarding "
                       "the one(s) provided by the simulation : using ";
          for (auto& script : pipelineScripts)
          {
            std::cout << script << " ";
          }
          std::cout << " instead of ";
          conduit_node* scriptsNode =
            conduit_node_fetch(catalystNode, scriptsPath.c_str());
          for (int i = 0; i < conduit_node_number_of_children(scriptsNode); i++)
          {
            std::cout << conduit_node_as_char8_str(
                           conduit_node_child(scriptsNode, i))
                      << " ";
          }
          std::cout << std::endl;

          // Discard scripts node
          conduit_node_remove_path(catalystNode, scriptsPath.c_str());
        }
        for (int i = 0; i < pipelineScripts.size(); i++)
        {
          std::string path = "catalyst/scripts/script" + std::to_string(i + 1);
          conduit_node_set_path_char8_str(
            catalystNode, path.c_str(), pipelineScripts.at(i).c_str());
        }

        // We force to use the catalyst specific implementation for paraview for the reader
        conduit_node_set_path_char8_str(
          catalystNode, "catalyst_load/implementation", "paraview");
        auto status = catalyst_initialize(catalystNode);
        return status;
      }
    }
  }

  std::cerr << "Catalyst Initialization failed" << std::endl;

  return catalyst_status::catalyst_status_error_incomplete;
}

//----------------------------------------------------------------------------
catalyst_status replay_adios_execute(SimulationSynchronizer& synchronizer)
{
  synchronizer.RetrieveAdiosVariables();

  synchronizer.GenerateOutputs();

  auto simuCpp = synchronizer.GetConduitNodes();
  if (simuCpp.size() == 1)
  {
    auto simu_conduit = conduit_cpp::c_node(&simuCpp[0]);
    return catalyst_execute(simu_conduit);
  }

  // Recreate a valid blueprint mesh with multimesh
  conduit_node* merged = conduit_node_create();
  conduit_cpp::Node merged_node = conduit_cpp::cpp_node(const_cast<conduit_node*>(merged));

  merged_node["catalyst/channels/input/type"] = "multimesh";
  for (int i = 0; i < simuCpp.size(); i++)
  {
    auto simu_conduit = simuCpp[i]["catalyst/channels"].child(0);
    std::string simuDatatype = simu_conduit["type"].as_string();

    // We can only aggregate mesh into a multimesh for now.
    bool isMesh = simuDatatype == "mesh";
    if (!isMesh)
    {
      continue;
    }

    merged_node["catalyst/state"].set_node(simuCpp[0]["catalyst/state"]);
    std::string simu_fullname = "catalyst/channels/input/data/simulation" + std::to_string(i);
    merged_node[simu_fullname].set_node(simu_conduit["data"]);
  }

  merged_node.print();

  return catalyst_execute(merged);
}

//----------------------------------------------------------------------------
bool replay_adios_finalize(SimulationSynchronizer& synchronizer)
{
  assert(synchronizer.GetNumberOfSimulations() > 0);

  conduit_node* catalystNode = conduit_node_create();
  std::string catalystNodeVariableName = "CatalystFinalizeParameters";
  const auto& catalystAttribute = synchronizer.GetIOAttribute(0, catalystNodeVariableName);

  if (catalystAttribute)
  {
    std::string catalystNodeJson = catalystAttribute.Data()[0];
    if (!catalystNodeJson.empty())
    {
      conduit_node_generate(catalystNode, catalystNodeJson.c_str(), "json", nullptr);
    }
  }

  return catalyst_finalize(catalystNode) == catalyst_status::catalyst_status_ok;
}

//----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  std::string xmlConfigFile;
  std::vector<std::string> sstFiles;
  std::vector<std::string> pipelineScripts;
  if (!::RetrieveFilesAsArgs(argc, argv, xmlConfigFile, sstFiles, pipelineScripts))
  {
    MPI_Finalize();
    return 0;
  }

  if (sstFiles.empty())
  {
    sstFiles.push_back("gs.bp");
  }

  SimulationSynchronizer synchronizer;
  for (std::size_t i = 0; i < sstFiles.size(); i++)
  {
    synchronizer.AddSimulation(xmlConfigFile, sstFiles[i]);
  }

  bool isInitialized = false;
  while(true)
  {
    synchronizer.BeginStep(adios2::StepMode::Read);

    if (!synchronizer.AreSimulationsSync())
    {
      synchronizer.EndStep();
      continue;
    }

    if (!synchronizer.AreSimulationsAvailable())
    {
      // Simulation is finished, exit.
      break;
    }

    if (!isInitialized)
    {
      auto status = replay_adios_initialize(synchronizer, pipelineScripts);
      if (status != catalyst_status::catalyst_status_ok)
      {
        synchronizer.Close();
        MPI_Finalize();
        return EXIT_FAILURE;
      }

      isInitialized = true;
    }

    if (replay_adios_execute(synchronizer) != catalyst_status::catalyst_status_ok)
    {
      synchronizer.Close();
      MPI_Finalize();
      return EXIT_FAILURE;
    }
  }

  if (!replay_adios_finalize(synchronizer))
  {
    std::cerr << "catalyst_finalize failed." << std::endl;
  }

  synchronizer.Close();

  MPI_Finalize();
  return 0;
}
