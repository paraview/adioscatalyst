#include "SimulationHelper.h"

#include <algorithm>

namespace
{

typedef SimulationHelper::ReplayData ReplayData;
typedef SimulationHelper::BlockMapping BlockMapping;

//----------------------------------------------------------------------------
// Used to debug conduit_node, print only the conduit hierarchy name without any details.
void PrintHierarchy(const conduit_cpp::Node root, const std::string& spacing = "")
{
  auto nbChild = root.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    const auto& child = root.child(i);
    std::cout << spacing << child.name() << std::endl;
    PrintHierarchy(child, spacing + " ");
  }
}

//----------------------------------------------------------------------------
// Used to know if the variableName is an information added for adios2 like cell_shape, cell_start,
// cell_count
bool IsAdiosMetaInformation(const std::string& variableName)
{
  bool isAdiosVariable = false;
  isAdiosVariable |= variableName.find("points_shape") != std::string::npos;
  isAdiosVariable |= variableName.find("points_start") != std::string::npos;
  isAdiosVariable |= variableName.find("points_count") != std::string::npos;

  isAdiosVariable |= variableName.find("cells_shape") != std::string::npos;
  isAdiosVariable |= variableName.find("cells_start") != std::string::npos;
  isAdiosVariable |= variableName.find("cells_count") != std::string::npos;

  return isAdiosVariable;
}

//----------------------------------------------------------------------------
std::string GetFieldAssociation(const ReplayData& fieldsData, const std::string& fieldName)
{
  std::string fieldAssociation = "None";
  for (const auto& variable : fieldsData.stringVariables)
  {
    if (variable.first.find(fieldName) != std::string::npos &&
      variable.first.find("association") != std::string::npos)
    {
      fieldAssociation = variable.second;
      break;
    }
  }

  return fieldAssociation;
}

//----------------------------------------------------------------------------
/**
 * Use the ADIOS variable to retrieve the writer side partitioning and determine
 * the reader side partitioning in a round-robin fashion.
 *
 * Each of the writer side blocks gets distributed to a reader MPI process
 * depending on these three cases:
 * - the writer and the reader have the same number of ranks: distribute blocks
 *   one to one
 * - the writer has more ranks than the reader: distribute blocks as equally as
 *   possible with lower rank reader processes having slightly higher priority
 * - the reader has more ranks than the writer: distribute blocks to lower rank
 *   reader processes and leave higher ranks empty
 *
 * Returns a pair with:
 * - first: an aggregate BlockMapping holding the global offset information to read
 *   into this rank for the given variable
 * - second: a list of BlockMappings each holding the a piece of information regarding
 *   the initial partitioning of the data on the writer side
 */
template <typename T>
std::pair<BlockMapping, std::vector<BlockMapping>> DetermineReadingDirectives(
  adios2::Variable<T>& adiosVariable, adios2::Engine& reader)
{
  auto blocksInfo = reader.BlocksInfo(adiosVariable, reader.CurrentStep());
  std::size_t nBlocks = blocksInfo.size();

  int rank = 0;
  int nRanks = 1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nRanks);

  double blocksPerRank = static_cast<double>(nBlocks) / static_cast<double>(nRanks);
  std::size_t thresholdRank = nBlocks % nRanks;
  std::size_t nBlocksLocal =
    (rank < thresholdRank) ? std::ceil(blocksPerRank) : std::floor(blocksPerRank);
  std::size_t startingBlock = (rank < thresholdRank)
    ? rank * nBlocksLocal
    : thresholdRank * std::ceil(blocksPerRank) + (rank - thresholdRank) * nBlocksLocal;

  std::size_t startRead =
    (blocksInfo[startingBlock].Start.size() > 0) ? blocksInfo[startingBlock].Start[0] : 0;
  std::size_t countRead = 0;
  std::vector<BlockMapping> originalBlocks(nBlocksLocal);
  for (std::size_t iB = 0; iB < nBlocksLocal; ++iB)
  {
    std::size_t blockOffset = startingBlock + iB;
    auto blockInfo = blocksInfo[blockOffset];
    BlockMapping subBlockMap;
    subBlockMap.BlockStart = blockOffset;
    subBlockMap.BlockCount = 1;
    subBlockMap.StartRead = (blockInfo.Start.size() > 0) ? blockInfo.Start[0] : 0;
    subBlockMap.CountRead = (blockInfo.Count.size() > 0) ? blockInfo.Count[0] : 1;
    originalBlocks[iB] = subBlockMap;
    countRead += subBlockMap.CountRead;
  }

  BlockMapping bMap;
  bMap.BlockStart = startingBlock;
  bMap.BlockCount = nBlocksLocal;
  bMap.StartRead = startRead;
  bMap.CountRead = countRead;
  return std::pair<BlockMapping, std::vector<BlockMapping>>{ bMap, originalBlocks };
}

//----------------------------------------------------------------------------
template <typename T>
void FillVariable(std::pair<std::string, adios2::Params> variable, adios2::Engine& reader,
  adios2::IO io, std::map<std::string, SimulationHelper::DataBlock<T>>& container)
{
  try
  {
    container.insert(std::pair<std::string, SimulationHelper::DataBlock<T>>(
      variable.first, SimulationHelper::DataBlock<T>()));
    auto& varBlock = container[variable.first];

    adios2::Variable<T> var = io.InquireVariable<T>(variable.first);
    auto blockMappingPair = DetermineReadingDirectives(var, reader);
    varBlock.AggregateBlock = blockMappingPair.first;
    varBlock.OriginBlocks = blockMappingPair.second;

    varBlock.Data.resize(varBlock.AggregateBlock.CountRead);

    if (varBlock.AggregateBlock.CountRead > varBlock.AggregateBlock.BlockCount)
    {
      var.SetSelection(adios2::Box<adios2::Dims>(
        { varBlock.AggregateBlock.StartRead }, { varBlock.AggregateBlock.CountRead }));
    }
    reader.Get<T>(var, varBlock.Data.data());
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
  }
}

//----------------------------------------------------------------------------
void FillContainer(ReplayData& data, adios2::Engine reader, adios2::IO io,
  std::pair<std::string, adios2::Params> variable)
{
  auto setting = variable.second;
  if (setting["Type"] == "string")
  {
    try
    {
      std::string var;
      reader.Get<std::string>(variable.first, var);
      if (!var.empty())
      {
        data.stringVariables.insert(std::pair<std::string, std::string>(variable.first, var));
      }
    }
    catch (const std::exception& e)
    {
      std::cerr << e.what() << std::endl;
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }
  }
  else if (setting["Type"] == "int8_t")
  {
    FillVariable<signed char>(variable, reader, io, data.charVariables);
  }
  else if (setting["Type"] == "int16_t")
  {
    FillVariable<short>(variable, reader, io, data.shortVariables);
  }
  else if (setting["Type"] == "int32_t")
  {
    FillVariable<int>(variable, reader, io, data.intVariables);
  }
  else if (setting["Type"] == "int64_t")
  {
    FillVariable<long int>(variable, reader, io, data.longIntVariables);
  }
  else if (setting["Type"] == "uint8_t")
  {
    FillVariable<unsigned char>(variable, reader, io, data.ucharVariables);
  }
  else if (setting["Type"] == "uint16_t")
  {
    FillVariable<unsigned short>(variable, reader, io, data.ushortVariables);
  }
  else if (setting["Type"] == "uint32_t")
  {
    FillVariable<unsigned int>(variable, reader, io, data.uintVariables);
  }
  else if (setting["Type"] == "uint64_t")
  {
    FillVariable<unsigned long int>(variable, reader, io, data.uLongIntVariables);
  }
  else if (setting["Type"] == "float")
  {
    FillVariable<float>(variable, reader, io, data.floatVariables);
  }
  else if (setting["Type"] == "double")
  {
    FillVariable<double>(variable, reader, io, data.doubleVariables);
  }
  else
  {
    std::cerr << "Unsupported type : " << setting["Type"] << " from " << variable.first
              << " -> Skip" << std::endl;
  }
}

//----------------------------------------------------------------------------
template <typename T>
const std::vector<BlockMapping>* FindFirstOriginBlocks(
  const std::map<std::string, SimulationHelper::DataBlock<T>>& data, const std::string& keyPart)
{
  for (const auto& pair : data)
  {
    if (pair.first.find(keyPart) != std::string::npos)
    {
      return &pair.second.OriginBlocks;
    }
  }
  return nullptr;
}

//----------------------------------------------------------------------------
const std::vector<BlockMapping>* FindFirstOriginBlocks(
  const ReplayData& data, const std::string& keyPart)
{

#define FindFirstOriginBlocksMacro(map)                                                            \
  do                                                                                               \
  {                                                                                                \
    if (auto res = FindFirstOriginBlocks(map, keyPart))                                            \
    {                                                                                              \
      return res;                                                                                  \
    }                                                                                              \
  } while (0)
  FindFirstOriginBlocksMacro(data.charVariables);
  FindFirstOriginBlocksMacro(data.shortVariables);
  FindFirstOriginBlocksMacro(data.intVariables);
  FindFirstOriginBlocksMacro(data.longIntVariables);
  FindFirstOriginBlocksMacro(data.ucharVariables);
  FindFirstOriginBlocksMacro(data.ushortVariables);
  FindFirstOriginBlocksMacro(data.uintVariables);
  FindFirstOriginBlocksMacro(data.uLongIntVariables);
  FindFirstOriginBlocksMacro(data.floatVariables);
  FindFirstOriginBlocksMacro(data.doubleVariables);
#undef FindFirstOriginBlocksMacro
  std::cerr << "Could not find match for : " << keyPart << std::endl;
  return nullptr;
}

//----------------------------------------------------------------------------
template <typename T>
void RectifyTopologyOffsets(
  SimulationHelper::DataBlock<T>& connectivity, const std::vector<BlockMapping>& pointBlockOffsets)
{
  const auto cellBlockOffsets = connectivity.OriginBlocks;
  if (cellBlockOffsets.size() != pointBlockOffsets.size())
  {
    std::cerr << "Number of point blocks and cell blocks don't line up" << std::endl;
  }

  if (cellBlockOffsets.size() < 2)
  {
    return;
  }

  std::size_t currentCellOffset = cellBlockOffsets[0].CountRead;
  std::size_t currentPointOffset = pointBlockOffsets[0].CountRead;
  for (std::size_t iB = 1; iB < cellBlockOffsets.size(); ++iB)
  {
    std::for_each(connectivity.Data.begin() + currentCellOffset,
      connectivity.Data.begin() + currentCellOffset + cellBlockOffsets[iB].CountRead,
      [&currentPointOffset](T& val) { val += currentPointOffset; });
    currentCellOffset += cellBlockOffsets[iB].CountRead;
    currentPointOffset += pointBlockOffsets[iB].CountRead;
  }
}

//----------------------------------------------------------------------------
void RectifyTopologyOffsets(ReplayData& data, const std::string& connectivityKey,
  const std::vector<BlockMapping>& pointBlockOffsets)
{
  if (data.charVariables.find(connectivityKey) != data.charVariables.end())
  {
    RectifyTopologyOffsets(data.charVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.shortVariables.find(connectivityKey) != data.shortVariables.end())
  {
    RectifyTopologyOffsets(data.shortVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.intVariables.find(connectivityKey) != data.intVariables.end())
  {
    RectifyTopologyOffsets(data.intVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.longIntVariables.find(connectivityKey) != data.longIntVariables.end())
  {
    RectifyTopologyOffsets(data.longIntVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.ucharVariables.find(connectivityKey) != data.ucharVariables.end())
  {
    RectifyTopologyOffsets(data.ucharVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.ushortVariables.find(connectivityKey) != data.ushortVariables.end())
  {
    RectifyTopologyOffsets(data.ushortVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.uintVariables.find(connectivityKey) != data.uintVariables.end())
  {
    RectifyTopologyOffsets(data.uintVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.uLongIntVariables.find(connectivityKey) != data.uLongIntVariables.end())
  {
    RectifyTopologyOffsets(data.uLongIntVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.floatVariables.find(connectivityKey) != data.floatVariables.end())
  {
    RectifyTopologyOffsets(data.floatVariables[connectivityKey], pointBlockOffsets);
  }
  else if (data.doubleVariables.find(connectivityKey) != data.doubleVariables.end())
  {
    RectifyTopologyOffsets(data.doubleVariables[connectivityKey], pointBlockOffsets);
  }
  else
  {
    std::cerr << "Unsupported key : " << connectivityKey << std::endl;
  }
}

//----------------------------------------------------------------------------
template <typename T>
void FillConduit(conduit_cpp::Node& node, const std::map<std::string, SimulationHelper::DataBlock<T>>& variables)
{
  for (const auto& variable : variables)
  {
    node[variable.first].set(variable.second.Data.data());
  }
}

//----------------------------------------------------------------------------
template <typename T>
void FillValuesInZeroCopy(
  conduit_cpp::Node& root, const std::map<std::string, SimulationHelper::DataBlock<T>>& variables)
{
  for (const auto& variable : variables)
  {
    // We doesn't want to put in the next Catalyst inmplementation property used only for Adios
    if (IsAdiosMetaInformation(variable.first))
    {
      continue;
    }

    if (variable.first.find("values") != std::string::npos ||
      variable.first.find("connectivity") != std::string::npos)
    {
      root[variable.first].set_external(variable.second.Data.data(), variable.second.Data.size());
    }
    else if (variable.first.find("coords/dims") != std::string::npos)
    {
      // Do nothing
    }
    else
    {
      root[variable.first].set(variable.second.Data.data());
    }
  }
}

//----------------------------------------------------------------------------
void FillCoordsetsInNode(conduit_cpp::Node& root, const ReplayData& coordsetsData)
{
  // String information don't need to be offset
  for (const auto& variable : coordsetsData.stringVariables)
  {
    root[variable.first] = variable.second;
  }

  FillValuesInZeroCopy(root, coordsetsData.ucharVariables);
  FillValuesInZeroCopy(root, coordsetsData.ushortVariables);
  FillValuesInZeroCopy(root, coordsetsData.uintVariables);
  FillValuesInZeroCopy(root, coordsetsData.uLongIntVariables);
  FillValuesInZeroCopy(root, coordsetsData.charVariables);
  FillValuesInZeroCopy(root, coordsetsData.shortVariables);
  FillValuesInZeroCopy(root, coordsetsData.intVariables);
  FillValuesInZeroCopy(root, coordsetsData.longIntVariables);
  FillValuesInZeroCopy(root, coordsetsData.floatVariables);
  FillValuesInZeroCopy(root, coordsetsData.doubleVariables);
}

//----------------------------------------------------------------------------
void FillTopologiesInNode(conduit_cpp::Node& root, const ReplayData& topologicalData)
{
  // String information don't need to have offset
  for (const auto& variable : topologicalData.stringVariables)
  {
    root[variable.first].set(variable.second);
  }

  FillValuesInZeroCopy(root, topologicalData.ucharVariables);
  FillValuesInZeroCopy(root, topologicalData.ushortVariables);
  FillValuesInZeroCopy(root, topologicalData.uintVariables);
  FillValuesInZeroCopy(root, topologicalData.uLongIntVariables);
  FillValuesInZeroCopy(root, topologicalData.charVariables);
  FillValuesInZeroCopy(root, topologicalData.shortVariables);
  FillValuesInZeroCopy(root, topologicalData.intVariables);
  FillValuesInZeroCopy(root, topologicalData.longIntVariables);
  FillValuesInZeroCopy(root, topologicalData.floatVariables);
  FillValuesInZeroCopy(root, topologicalData.doubleVariables);
}

//----------------------------------------------------------------------------
void FillFieldsInNode(
  conduit_cpp::Node& root, const ReplayData& fieldsData, const std::string& fieldName)
{
  std::string fieldAssociation = GetFieldAssociation(fieldsData, fieldName);

  for (auto variable : fieldsData.stringVariables)
  {
    root[variable.first] = variable.second;
  }

  FillValuesInZeroCopy(root, fieldsData.ucharVariables);
  FillValuesInZeroCopy(root, fieldsData.ushortVariables);
  FillValuesInZeroCopy(root, fieldsData.uintVariables);
  FillValuesInZeroCopy(root, fieldsData.uLongIntVariables);
  FillValuesInZeroCopy(root, fieldsData.charVariables);
  FillValuesInZeroCopy(root, fieldsData.shortVariables);
  FillValuesInZeroCopy(root, fieldsData.intVariables);
  FillValuesInZeroCopy(root, fieldsData.longIntVariables);
  FillValuesInZeroCopy(root, fieldsData.floatVariables);
  FillValuesInZeroCopy(root, fieldsData.doubleVariables);
}

//----------------------------------------------------------------------------
void FillNode(conduit_cpp::Node& node, const ReplayData& container)
{
  FillConduit(node, container.charVariables);
  FillConduit(node, container.shortVariables);
  FillConduit(node, container.intVariables);
  FillConduit(node, container.longIntVariables);
  FillConduit(node, container.ucharVariables);
  FillConduit(node, container.ushortVariables);
  FillConduit(node, container.uintVariables);
  FillConduit(node, container.uLongIntVariables);
  FillConduit(node, container.floatVariables);
  FillConduit(node, container.doubleVariables);
  for (auto variable : container.stringVariables)
  {
    node[variable.first].set(variable.second);
  }
}
}

//----------------------------------------------------------------------------
SimulationHelper::SimulationHelper() = default;

//----------------------------------------------------------------------------
SimulationHelper::SimulationHelper(const std::string& configFile, const std::string& sstFileName, MPI_Comm comm)
  : AdiosConfigFile(configFile)
  , SSTFileName(sstFileName)
{
  this->Adios = adios2::ADIOS(this->AdiosConfigFile, comm);
  this->Io = this->Adios.DeclareIO("Reader");
  this->Engine = this->Io.Open(this->SSTFileName, adios2::Mode::Read, comm);
}

//----------------------------------------------------------------------------
std::size_t SimulationHelper::GetCurrentStep() const
{
  return this->Engine.CurrentStep();
}

//----------------------------------------------------------------------------
conduit_cpp::Node SimulationHelper::GetOutputHandler() const
{
  return this->OutputHandler;
}

//----------------------------------------------------------------------------
std::string SimulationHelper::GetConduitChannelType() const
{
  if (conduit_cpp::c_node(&this->OutputHandler) == nullptr)
  {
    return "";
  }

  auto simuDataType = this->OutputHandler["catalyst/channels"].child(0);
  return simuDataType["type"].as_string();
}

//----------------------------------------------------------------------------
bool SimulationHelper::IsValid() const
{
  bool isValid = true;

  isValid &= conduit_cpp::c_node(&this->OutputHandler) != nullptr;
  isValid &= this->GetCurrentStep() != -1;

  return isValid;
}

//----------------------------------------------------------------------------
void SimulationHelper::BeginStep(const adios2::StepMode& mode)
{
  this->StepStatus = this->Engine.BeginStep(mode);
}

//----------------------------------------------------------------------------
void SimulationHelper::EndStep()
{
  this->Engine.EndStep();
}

//----------------------------------------------------------------------------
void SimulationHelper::Close()
{
  this->Engine.Close();
}

//----------------------------------------------------------------------------
void SimulationHelper::RetrieveAdiosVariables()
{
  auto variables = this->Io.AvailableVariables();
  for (const auto& variable : variables)
  {
    adios2::Params setting = variable.second;

    if (setting.count("Type") == 0)
    {
      std::cerr << "All variable should have a 'Type' field but not " << variable.first
                << std::endl;
      continue;
    }

    // All of this part will be
    if (variable.first.find("catalyst/channels/") != std::string::npos)
    {
      if (variable.first.find("coordsets") != std::string::npos)
      {
        ::FillContainer(this->CoordsetsData, this->Engine, this->Io, variable);
      }
      else if (variable.first.find("topologies") != std::string::npos)
      {
        ::FillContainer(this->TopologicalData, this->Engine, this->Io, variable);
        if (variable.first.find("connectivity") != std::string::npos)
        {
          this->ConnectivityKeys.emplace_back(variable.first);
        }
      }
      else if (variable.first.find("fields") != std::string::npos)
      {
        // extract the field name
        int fieldPos = variable.first.find("fields");
        int slashPos = variable.first.find("/", fieldPos);
        std::string subFields = variable.first.substr(slashPos + 1);
        std::string fieldName = subFields.substr(0, subFields.find("/"));

        if (fieldName.empty())
        {
          continue;
        }

        if (this->AllFieldsData.find(fieldName) == this->AllFieldsData.end())
        {
          ReplayData newFieldData;
          ::FillContainer(newFieldData, this->Engine, this->Io, variable);

          this->AllFieldsData.insert(std::pair<std::string, ReplayData>(fieldName, newFieldData));
        }
        else
        {
          ReplayData& fieldData = this->AllFieldsData.find(fieldName)->second;
          ::FillContainer(fieldData, this->Engine, this->Io, variable);
        }
      }
      else
      {
        ::FillContainer(this->MetaData, this->Engine, this->Io, variable);
      }
    }
    else if (variable.first.find("catalyst/state") != std::string::npos)
    {
      ::FillContainer(this->StatesData, this->Engine, this->Io, variable);
    }
    else
    {
      // Mesh blueprint can also have particules node etc...
      std::cerr << "unsupported variable named '" << variable.first << "'." << std::endl;
    }
  }

  this->Engine.EndStep();
}

//----------------------------------------------------------------------------
void SimulationHelper::GenerateOutput()
{
  for (auto connectivityKey : this->ConnectivityKeys)
  {
    std::string prefix = connectivityKey.substr(0, connectivityKey.find("topologies"));
    std::string namePrefix = connectivityKey.substr(0, connectivityKey.find("elements"));
    std::string coordsetsKeyPart = prefix + "coordsets/" +
      this->TopologicalData.stringVariables.find(namePrefix + "coordset")->second + "/values";

    const auto* pointOriginBlocks = ::FindFirstOriginBlocks(this->CoordsetsData, coordsetsKeyPart);
    if (!pointOriginBlocks)
    {
      std::cerr << "Failed to find origin blocks for coordset: " << coordsetsKeyPart << std::endl;
      return;
    }
    ::RectifyTopologyOffsets(this->TopologicalData, connectivityKey, *pointOriginBlocks);
  }

  // Populate the node with all data that didn't need offset, stride...
  ::FillNode(this->OutputHandler, this->StatesData);
  ::FillNode(this->OutputHandler, this->MetaData);

  // Coordsets, topologies and fields need more specific definition
  // First we need to know each node dimension
  ::FillCoordsetsInNode(this->OutputHandler, this->CoordsetsData);
  ::FillTopologiesInNode(this->OutputHandler, this->TopologicalData);

  for (const auto& pair : this->AllFieldsData)
  {
    ::FillFieldsInNode(this->OutputHandler, pair.second, pair.first);
  }
}

//----------------------------------------------------------------------------
void SimulationHelper::PrintOutput(bool withDetails)
{
  if (conduit_cpp::c_node(&this->OutputHandler) == nullptr)
  {
    std::cout << "There is no output to print." << std::endl;
    return;
  }

  if (withDetails)
  {
    this->OutputHandler.print_detailed();
  }
  else
  {
    this->OutputHandler.print();
  }
}
