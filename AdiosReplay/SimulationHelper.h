#ifndef SimulationHelper_h
#define SimulationHelper_h

#include <adios2.h>

#include <catalyst.hpp>

#include <mpi.h>

#include <string>

/**
 * @class SimulationHelper
 * @brief Handle all informations related to a specific simulation.
 *
 * SimulationHelper is responsible of a simulation and its related information like meta data (xml
 * config file,...).
 */
class SimulationHelper
{
public:
  SimulationHelper();
  ~SimulationHelper() = default;

  /**
   * Container used to map ADIOS blocks to current process
   */
  struct BlockMapping
  {
    std::size_t BlockStart = 0;
    std::size_t BlockCount = 0;
    std::size_t StartRead = 0;
    std::size_t CountRead = 0;
  };

  /**
   * Structure holding data and its original block description
   */
  template <typename DataT>
  struct DataBlock
  {
    BlockMapping AggregateBlock;
    std::vector<BlockMapping> OriginBlocks;
    std::vector<DataT> Data;
  };

  /**
   * Container used to store separetely all sub node data for 'channels', 'fields', 'topologies'.
   */
  struct ReplayData
  {
    std::map<std::string, std::string> stringVariables;
    std::map<std::string, DataBlock<signed char>> charVariables;
    std::map<std::string, DataBlock<short>> shortVariables;
    std::map<std::string, DataBlock<int>> intVariables;
    std::map<std::string, DataBlock<long int>> longIntVariables;
    std::map<std::string, DataBlock<unsigned char>> ucharVariables;
    std::map<std::string, DataBlock<unsigned short>> ushortVariables;
    std::map<std::string, DataBlock<unsigned int>> uintVariables;
    std::map<std::string, DataBlock<unsigned long int>> uLongIntVariables;
    std::map<std::string, DataBlock<float>> floatVariables;
    std::map<std::string, DataBlock<double>> doubleVariables;
  };

  /**
   * Construct a new Simulation Handler object
   */
  SimulationHelper(const std::string& configFile, const std::string& sstFileName, MPI_Comm comm);

  /**
   * Get the output node.
   *
   * Get the C version by default as it's required for the catalyst method.
   * For more processing on the output, please use the OutputHandler.
   */
  conduit_node* GetOutput() const;

  /**
   * Get the output conduit node wrapped, more convenient for additional treatment.
   */
  conduit_cpp::Node GetOutputHandler() const;

  ///@{
  /**
   * Get the IO/Engine.
   */
  adios2::IO GetIO() { return this->Io; }
  adios2::Engine GetEngine() { return this->Engine; }
  ///@}

  /**
   * Return the current timestep of the handled simulation.
   *
   * If the ADIOS Engine isn't ready, this method will return -1.
   */
  std::size_t GetCurrentStep() const;

  /**
   * Get the status of the current step.
   */
  adios2::StepStatus GetStepStatus() const { return this->StepStatus; }

  /**
   * Get the conduit mesh type. It could be "mesh", "multimesh".
   *
   * Empty otherwise.
   */
  std::string GetConduitChannelType() const;

  /**
   * Return if the reader adios2 is currently connected and the conduit node is generated.
   */
  bool IsValid() const;

  /**
   * Start a step computation.
   */
  void BeginStep(const adios2::StepMode& mode);

  /**
   * Finish the current step of the adios2 engine.
   */
  void EndStep();

  /**
   * Close the adios2 engine.
   */
  void Close();

  /**
   * Retrieve all adios2 variables for the current step.
   *
   * @warning: due to adios2 logic, this method requires to be between a call of BeginStep() and
   * EndStep().
   */
  void RetrieveAdiosVariables();

  /**
   * Based on available adios variables, generate the ouput node for the current step.
   *
   * @see RetrieveAdiosVariables()
   *
   * @warning data will be only avaiable after a EndStep() call.
   */
  void GenerateOutput();

  /**
   * Print the output conduit node.
   */
  void PrintOutput(bool withDetails = false);


private:
  SimulationHelper(const SimulationHelper&) = delete;
  void operator=(const SimulationHelper&) = delete;

  // Variables to handle the Adios2 part
  std::string AdiosConfigFile;
  std::string SSTFileName = "gs.bp";

  adios2::ADIOS Adios;
  adios2::IO Io;
  adios2::Engine Engine;

  ReplayData MetaData;
  ReplayData StatesData;
  ReplayData CoordsetsData;
  ReplayData TopologicalData;

  std::map<std::string, ReplayData> AllFieldsData;
  std::vector<std::string> ConnectivityKeys;

  adios2::StepStatus StepStatus = adios2::StepStatus::NotReady;
  catalyst_status CatalystStatus = catalyst_status::catalyst_status_ok;
  std::size_t CurrentStep = -1;

  conduit_cpp::Node OutputHandler;
};

#endif
