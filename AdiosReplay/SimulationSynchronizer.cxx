#include "SimulationSynchronizer.h"

#include "SimulationHelper.h"

//----------------------------------------------------------------------------
SimulationSynchronizer::SimulationSynchronizer() = default;

//----------------------------------------------------------------------------
SimulationSynchronizer::~SimulationSynchronizer() = default;

//----------------------------------------------------------------------------
void SimulationSynchronizer::AddSimulation(const std::string& xmlFile, const std::string& sstFile)
{
  this->Simulations.push_back(
    std::unique_ptr<SimulationHelper>(new SimulationHelper(xmlFile, sstFile, MPI_COMM_WORLD)));
}

//----------------------------------------------------------------------------
void SimulationSynchronizer::BeginStep(const adios2::StepMode& mode)
{
  for (auto& simu : this->Simulations)
  {
    simu->BeginStep(mode);
  }
}

//----------------------------------------------------------------------------
void SimulationSynchronizer::EndStep()
{
  for (auto& simu : this->Simulations)
  {
    simu->EndStep();
  }
}

//----------------------------------------------------------------------------
bool SimulationSynchronizer::AreSimulationsSync()
{
  bool sameCurrentStep = true;

  std::size_t currentStep = -1;
  for (auto& simu : this->Simulations)
  {
    auto step = simu->GetCurrentStep();
    if (currentStep == -1)
    {
      currentStep = step;
    }
    else if (currentStep != step)
    {
      return false;
    }
  }

  return sameCurrentStep;
}

//----------------------------------------------------------------------------
bool SimulationSynchronizer::AreSimulationsAvailable()
{
  bool allReady = true;
  for (auto const& simu : this->Simulations)
  {
    allReady &= simu->GetStepStatus() == adios2::StepStatus::OK;
  }

  return allReady;
}

//----------------------------------------------------------------------------
void SimulationSynchronizer::Close()
{
  for (auto& simu : this->Simulations)
  {
    simu->Close();
  }
}

//----------------------------------------------------------------------------
adios2::IO SimulationSynchronizer::GetIO(int index)
{
  return this->Simulations[index]->GetIO();
}

//----------------------------------------------------------------------------
adios2::Attribute<std::string> SimulationSynchronizer::GetIOAttribute(
  int simulationIdx, const std::string& attributeName)
{
  adios2::IO io = this->Simulations[simulationIdx]->GetIO();

  adios2::Attribute<std::string> catalystAttribute =
    io.InquireAttribute<std::string>(attributeName.c_str());

  return catalystAttribute;
}

//----------------------------------------------------------------------------
void SimulationSynchronizer::RetrieveAdiosVariables()
{
  for (auto& simu : this->Simulations)
  {
    simu->RetrieveAdiosVariables();
  }
}

//----------------------------------------------------------------------------
void SimulationSynchronizer::GenerateOutputs()
{
  for (auto& simu : this->Simulations)
  {
    simu->GenerateOutput();
  }
}

//----------------------------------------------------------------------------
std::vector<conduit_cpp::Node> SimulationSynchronizer::GetConduitNodes()
{
  std::vector<conduit_cpp::Node> outputs;

  for (auto& simu : this->Simulations)
  {
    outputs.push_back(simu->GetOutputHandler());
  }

  return outputs;
}

//----------------------------------------------------------------------------
void SimulationSynchronizer::PrintOutputs(bool withDetails)
{
  for (auto const& simu : this->Simulations)
  {
    simu->PrintOutput(withDetails);
  }
}
