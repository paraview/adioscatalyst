#ifndef SimulationSynchronizer_h
#define SimulationSynchronizer_h

#include <catalyst_conduit.hpp>

#include <adios2.h>

#include <vector>
#include <memory>

class SimulationHelper;

/**
 * @class SimulationSynchronizer
 * @brief Manage each simulation with other.
 *
 * SimulationSynchronizer is reponsible to register each simulation and synchronize them.
 */
class SimulationSynchronizer
{
public:
  SimulationSynchronizer();
  ~SimulationSynchronizer();

  /**
   * Add a new simulation at the end of the list in the manager.
   */
  void AddSimulation(const std::string& xmlFile, const std::string& sstFile);

  /**
   * Add a new simulation at the end of the list in the manager.
   */
  int GetNumberOfSimulations() { return this->Simulations.size(); }

  /**
   * Start a step computation for each simulation.
   */
  void BeginStep(const adios2::StepMode& mode);

  /**
   * Close the currentStep for each simulation.
   */
  void EndStep();

  /**
   * Call for each simulation 'close()' method to close the adios engine.
   */
  void Close();

  /**
   * Return true if each simulation is at the same step.
   */
  bool AreSimulationsSync();

  /**
   * Return true if each simulation has a StepStatus equal to "OK".
   */
  bool AreSimulationsAvailable();

  /**
   * Return the adios2::IO at the desired index in the list.
   */
  adios2::IO GetIO(int index = 0);

  /**
   * Inquire a specified attribute to the IO.
   */
  adios2::Attribute<std::string> GetIOAttribute(
    int simulationIdx, const std::string& attributeName);

  /**
   * Call 'RetrieveAdiosVariables' on each simulation gathered.
   */
  void RetrieveAdiosVariables();

  /**
   * Call for each simulation a 'generateOutput()'.
   */
  void GenerateOutputs();

  ///@{
  /**
   * Get the conduit node from the adios engine.
   */
  std::vector<conduit_cpp::Node> GetConduitNodes();
  ///@}

  /**
   * Call print output for each simulations.
   */
  void PrintOutputs(bool withDetails);

private:
  SimulationSynchronizer(const SimulationSynchronizer&) = delete;
  void operator=(const SimulationSynchronizer&) = delete;

  std::vector<std::unique_ptr<SimulationHelper>> Simulations;
};

#endif
