# ---------------------------------------------------------------------------------------
# Convenience function used to define a python test for adios2.
# Set required catalyst environment variables.
function(add_python_test)
  set(options)
  set(executableArgs
      NAME
  )

  set(oneValueArgs SCRIPT)
  cmake_parse_arguments(ARGS "${options}" "${executableArgs}" "${oneValueArgs}" "${ARGN}")
  add_test(NAME ${ARGS_NAME}
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/${ARGS_SCRIPT}
  )

  set_property(TEST ${ARGS_NAME} PROPERTY
    ENVIRONMENT
      "PYTHONPATH=$ENV{PYTHONPATH}"
    TIMEOUT 30
  )
endfunction()
