#include "DataStructures.h"

#include <cmath>
#include <iostream>
#include <iterator>
#include <numeric>

#include <mpi.h>

#include <fstream>

Grid::Grid() = default;

void Grid::Initialize(
  const unsigned int numPoints[3], const double spacing[3], const double position[3])
{
  if (numPoints[0] == 0 || numPoints[1] == 0 || numPoints[2] == 0)
  {
    std::cerr << "Must have a non-zero amount of points in each direction.\n";
  }

  this->Dims[0] = numPoints[0];
  this->Dims[1] = numPoints[1];
  this->Dims[2] = numPoints[2];

  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);

  if (mpiSize == 0)
  {
    std::cerr << "MPI world size is equal to 0 which should not be possible" << std::endl;
    return;
  }

  bool extraEntities = mpiRank < numPoints[0] % mpiSize;
  std::size_t numberOfXSlicesOnRank = std::floor(numPoints[0] / mpiSize);
  std::size_t startXGlobal =
    numberOfXSlicesOnRank * mpiRank + (extraEntities ? mpiRank : numPoints[0] % mpiSize);
  numberOfXSlicesOnRank += (extraEntities ? 2 : 1);

  std::size_t numberOfPointsOnRank = numberOfXSlicesOnRank * numPoints[1] * numPoints[2];

  this->Points.resize(numberOfPointsOnRank * 3);
  this->PointsCount = numberOfPointsOnRank;
  for (std::size_t iX = 0; iX < numberOfXSlicesOnRank; ++iX)
  {
    double coordX = position[0] + (startXGlobal + iX) * spacing[0];
    for (std::size_t iY = 0; iY < numPoints[1]; ++iY)
    {
      double coordY = position[1] + iY * spacing[1];
      for (std::size_t iZ = 0; iZ < numPoints[2]; ++iZ)
      {
        double coordZ = position[2] + iZ * spacing[2];
        std::size_t off = ((iX * numPoints[1] + iY) * numPoints[2] + iZ) * 3;
        this->Points[off] = coordX;
        this->Points[off + 1] = coordY;
        this->Points[off + 2] = coordZ;
      }
    }
  }

  this->CellsCount = (numberOfXSlicesOnRank - 1) * (numPoints[1] - 1) * (numPoints[2] - 1);

  for (unsigned int iX = 0; iX < numberOfXSlicesOnRank - 1; ++iX)
  {
    for (unsigned int iY = 0; iY < numPoints[1] - 1; ++iY)
    {
      for (unsigned int iZ = 0; iZ < numPoints[2] - 1; ++iZ)
      {
        std::vector<unsigned int> cellPts = { (iX * numPoints[1] + iY) * numPoints[2] + iZ,
          ((iX + 1) * numPoints[1] + iY) * numPoints[2] + iZ,
          ((iX + 1) * numPoints[1] + (iY + 1)) * numPoints[2] + iZ,
          (iX * numPoints[1] + (iY + 1)) * numPoints[2] + iZ,
          (iX * numPoints[1] + iY) * numPoints[2] + (iZ + 1),
          ((iX + 1) * numPoints[1] + iY) * numPoints[2] + (iZ + 1),
          ((iX + 1) * numPoints[1] + (iY + 1)) * numPoints[2] + (iZ + 1),
          (iX * numPoints[1] + (iY + 1)) * numPoints[2] + (iZ + 1) };
        std::copy(cellPts.begin(), cellPts.end(), std::back_inserter(this->Cells));
      }
    }
  }

  // communicate here to get shape and starts
  std::vector<unsigned int> allPoints(mpiSize, 0);
  MPI_Allgather(
    &this->PointsCount, 1, MPI_UNSIGNED, allPoints.data(), 1, MPI_UNSIGNED, MPI_COMM_WORLD);
  this->PointsStart = std::accumulate(allPoints.begin(), allPoints.begin() + mpiRank, 0);
  this->PointsShape =
    std::accumulate(allPoints.begin() + mpiRank, allPoints.end(), this->PointsStart);

  std::vector<unsigned int> allCells(mpiSize, 0);
  MPI_Allgather(
    &this->CellsCount, 1, MPI_UNSIGNED, allCells.data(), 1, MPI_UNSIGNED, MPI_COMM_WORLD);
  this->CellsStart = std::accumulate(allCells.begin(), allCells.begin() + mpiRank, 0);
  this->CellsShape = std::accumulate(allCells.begin() + mpiRank, allCells.end(), this->CellsStart);
}

size_t Grid::GetNumberOfPoints()
{
  return this->Points.size() / 3;
}

size_t Grid::GetNumberOfCells()
{
  return this->Cells.size() / 8;
}

double* Grid::GetPointsArray()
{
  if (this->Points.empty())
  {
    return nullptr;
  }
  return &(this->Points[0]);
}

double* Grid::GetPoint(size_t pointId)
{
  if (pointId >= this->Points.size())
  {
    return nullptr;
  }
  return &(this->Points[pointId * 3]);
}

unsigned int* Grid::GetCellPoints(size_t cellId)
{
  if (cellId >= this->Cells.size())
  {
    return nullptr;
  }
  return &(this->Cells[cellId * 8]);
}

int Grid::GetCellsShape()
{
  return this->CellsShape;
}

int Grid::GetCellsStart()
{
  return this->CellsStart;
}

int Grid::GetCellsCount()
{
  return this->CellsCount;
}

int Grid::GetPointsShape()
{
  return this->PointsShape;
}

int Grid::GetPointsStart()
{
  return this->PointsStart;
}

int Grid::GetPointsCount()
{
  return this->PointsCount;
}

size_t* Grid::GetDims()
{
  return this->Dims;
}

Attributes::Attributes()
{
  this->GridPtr = nullptr;
}

void Attributes::Initialize(Grid* grid)
{
  this->GridPtr = grid;
}

void Attributes::UpdateFields(double time)
{
  size_t numPoints = this->GridPtr->GetNumberOfPoints();
  this->Velocity.resize(numPoints * 3);
  for (size_t pt = 0; pt < numPoints; pt++)
  {
    double* coord = this->GridPtr->GetPoint(pt);
    this->Velocity[pt] = coord[0] * time;
    this->Velocity[pt + numPoints] = coord[0] + coord[1] * time;
    this->Velocity[pt + numPoints * 2] = coord[0] + coord[2] * (time);
  }
  size_t numCells = this->GridPtr->GetNumberOfCells();

  this->Pressure.resize(numCells);
  std::fill(this->Pressure.begin(), this->Pressure.end(), 1);
}

double* Attributes::GetVelocityArray()
{
  if (this->Velocity.empty())
  {
    return nullptr;
  }
  return &this->Velocity[0];
}

float* Attributes::GetPressureArray()
{
  if (this->Pressure.empty())
  {
    return nullptr;
  }
  return &this->Pressure[0];
}

void Attributes::SetSSTFileName(const std::string& fileName)
{
  this->SSTFileName = fileName;
}
