#ifndef DataStructures_HEADER
#define DataStructures_HEADER

#include <cstddef>
#include <vector>
#include <string>

class Grid
{
public:
  Grid();
  void Initialize(
    const unsigned int numPoints[3], const double spacing[3], const double position[3]);
  size_t GetNumberOfPoints();
  size_t GetNumberOfCells();
  double* GetPointsArray();
  double* GetPoint(size_t pointId);
  size_t* GetDims();
  unsigned int* GetCellPoints(size_t cellId);

  /**
   * Specific information required by AdiosCatalyst implementation
   */
  int GetCellsShape();
  int GetCellsStart();
  int GetCellsCount();
  int GetPointsShape();
  int GetPointsStart();
  int GetPointsCount();

private:
  std::vector<double> Points;
  std::vector<unsigned int> Cells;
  std::vector<unsigned int> CellsOffsetted;

  unsigned int CellsShape = 0;
  unsigned int CellsStart = 0;
  unsigned int CellsCount = 0;

  unsigned int PointsShape = 0;
  unsigned int PointsStart = 0;
  unsigned int PointsCount = 0;

  size_t Dims[3] = { 0, 0, 0 };
};

class Attributes
{
  // A class for generating and storing point and cell fields.
  // Velocity is stored at the points and pressure is stored
  // for the cells. The current velocity profile is for a
  // flow with U(y,t) = y*t, V = 0 and W = y + z * t.
  // Pressure is constant through the domain.
public:
  Attributes();
  void Initialize(Grid* grid);
  void UpdateFields(double time);
  double* GetVelocityArray();
  float* GetPressureArray();

  void SetSSTFileName(const std::string& fileName);
  std::string GetSSTFileName() const { return SSTFileName; };

private:
  std::vector<double> Velocity;
  std::vector<float> Pressure;
  Grid* GridPtr;

  std::string SSTFileName = "gs.bp";
};
#endif
