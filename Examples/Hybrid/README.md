# AdiosCatalyst example: hybrid in-transit/in-situ

This example shows how an hybrid in-transit/in-situ AdiosCatalyst pipeline is used. The idea behind this method is to combine the in-transit pipeline with an in-situ analysis : before sending the data through ADIOS to the visualization node, AdiosCatalyst will use Paraview Catalyst to operate a data reduction. The reduced data is retrieved by AdiosCatalyst from Paraview Catalyst using the steering extractor mechanism, serialized and finally sent to the visualization node using ADIOS2. This approach is useful when using large datasets: the synchronous in-situ script reduces the size of the data without doing any RAM copy, then the reduced data is sent over the network by ADIOS2 to be post-processed in a visualization node asynchronously. 

In practice, it means that the simulation node that calls AdiosCatalyst will need to provide a data reduction pipeline script. It also needs to have access to the Paraview Catalyst implementation.

Note : for now, the multimesh protocol is not compatible with the hybrid in-transi/in-situ workflow.

## Running the example

**NB:** this example requires using Paraview master branch after commit with hash [60ffd022](https://gitlab.kitware.com/paraview/paraview/-/commit/60ffd022f6e1e8ba6c17d8e39fbf4645cdd24438) (or 02/12/23 nightly build or after).

In this scenario, the simulation needs to access AdiosCatalyst implementation and AdiosCatalyst implementation needs access to ParaviewCatalyst. We need to specify the path to both in the environment variable `CATALYST_IMPLEMENTATION_PATHS`, separated by ":". The simulation `Example_hybrid` will use the first Python script in command line arguments as a reduction script for AdiosCatalyst, and the second one for the visualization pipeline on the visualization node. Note that this second script could also be specified in the `AdiosReplay` command invocation.

```bash
export CATALYST_IMPLEMENTATION_PATHS="<path_to_libcatalyst-adios.so>:<path_to_libcatalyst-paraview.so>"
bin/Example_Hybrid ../Examples/Commons/adios2.xml ../Examples/Hybrid/reduction_pipeline.py
```

On another terminal, start the visualization node. This process only needs to know about the Paraview Catalyst implementation.

```bash
export CATALYST_IMPLEMENTATION_PATHS="<paraview-build-location>/lib/catalyst"
bin/AdiosReplay ../Examples/Commons/adios2.xml ../Examples/Hybrid/visualization_pipeline.py
```
