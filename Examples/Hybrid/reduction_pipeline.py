from paraview.simple import *

print("Executing data reduction pipeline")

from paraview import catalyst
options = catalyst.Options()

# registrationName must match the channel name used in the
# 'CatalystAdaptor'.
producer = TrivialProducer(registrationName="grid")

# Create a slice of the producer
sliced = Slice(Input=producer, SliceType="Plane")
sliced.SliceType.Offset = 0.0
sliced.SliceType.Origin = [60.0, 35.4, 62.35]
sliced.SliceType.Normal = [0.0, 1.0, 0.0]

# Create an extractor taking the output of the slice. This way, 
# the slice will be sent back as a Catalyst result node to AdiosCatalyst.
steering = CreateExtractor('steering', sliced, registrationName='steerchannel')


def catalyst_execute(info):
    global sliced
    sliced.UpdatePipeline()
    print("-----------------------------------")
    print("executing (cycle={}, time={})".format(info.cycle, info.time))
    print("bounds:", producer.GetDataInformation().GetBounds())
