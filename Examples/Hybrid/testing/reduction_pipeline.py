from paraview.simple import *

print("Executing data reduction pipeline")

from paraview import catalyst
options = catalyst.Options()


# registrationName must match the channel name used in the
# 'CatalystAdaptor'.
producer = TrivialProducer(registrationName="grid")

# Create a slice of the producer
sliced = Slice(Input=producer, SliceType="Plane")
sliced.SliceType.Offset = 0.0
sliced.SliceType.Origin = [60.0, 35.4, 62.35]
sliced.SliceType.Normal = [0.0, 1.0, 0.0]

# Create an extractor taking the output of the slice. This way,
# the slice will be sent back as a Catalyst result node to AdiosCatalyst.
steering = CreateExtractor("steering", sliced, registrationName="steerchannel")


def catalyst_execute(info):
    sliced.UpdatePipeline()

    producerInformation = producer.GetDataInformation()
    bounds = producer.GetDataInformation().GetBounds()

    # Detect MPI and rank based on bounds
    is_rank0 = bounds[0] == 0.0 and bounds[1] == 60.0
    is_rank1 = bounds[0] == 60.0 and bounds[1] == 120.0
    is_mpi = is_rank0 or is_rank1

    if not is_mpi:
        assert (
            producerInformation.GetNumberOfCells() == 304440
        ), "Producer does not have the right number of cells"
        assert (
            producerInformation.GetNumberOfPoints() == 319440
        ), "Producer does not have the right number of points"
    else:
        assert (
            producerInformation.GetNumberOfCells() == 152220
        ), "Producer does not have the right number of cells"
        assert (
            producerInformation.GetNumberOfPoints() == 161040
        ), "Producer does not have the right number of points"
    
    sliceInformation = sliced.GetDataInformation()
    if not is_mpi:
        assert (
            sliceInformation.GetNumberOfCells() == 10320
        ), "Sliced data does not have the right number of cells"
        assert (
            sliceInformation.GetNumberOfPoints() == 5324
        ), "Sliced data does not have the right number of points"
    else:
        assert (
            sliceInformation.GetNumberOfCells() == 5160
        ), "Sliced data does not have the right number of cells"
        assert (
            sliceInformation.GetNumberOfPoints() == 2684
        ), "Sliced data does not have the right number of points"
