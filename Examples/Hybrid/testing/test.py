#!/usr/bin/env python
import subprocess
from subprocess import PIPE
import sys
import os

os.chdir(os.path.dirname(__file__))

# Retrieve simulation executables
build_folder = sys.argv[1]
simulation_executable = build_folder + "/bin/Example_Hybrid"
visualization_executable = build_folder + "/bin/AdiosReplay"

simulation_command = ""
visualization_command = ""

# Add the MPI prefix if MPI is active
if len(sys.argv) == 5:
    simulation_command += " mpirun -np 2"
    visualization_command += " mpirun -np 2"

simulation_command += (
    f" {simulation_executable} ../../Commons/adios2.xml reduction_pipeline.py"
)
visualization_command += (
    f" {visualization_executable} ../../Commons/adios2.xml visualization_pipeline.py"
)

simulation_env = os.environ.copy()
simulation_env["CATALYST_IMPLEMENTATION_PATHS"] = f"{sys.argv[2]}:{sys.argv[3]}"
visualization_env = os.environ.copy()
visualization_env["CATALYST_IMPLEMENTATION_PATHS"] = sys.argv[3]

simulation_process = subprocess.Popen(
    simulation_command, stdout=PIPE, stderr=PIPE, shell=True, env=simulation_env
)

visualization_process = subprocess.Popen(
    visualization_command, stdout=PIPE, stderr=PIPE, shell=True, env=visualization_env
)

stdout, stderr = simulation_process.communicate()
stdout2, stderr2 = visualization_process.communicate()

assert stderr == b"", f"Error on simulation side: {stderr}"
assert stderr2 == b"", f"Error on visualization side: {stderr2}"
