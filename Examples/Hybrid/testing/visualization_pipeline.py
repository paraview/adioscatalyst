from paraview.simple import *

print("Executing visualization pipeline on reduced data")

from paraview import catalyst

options = catalyst.Options()
options.GlobalTrigger = "TimeStep"
options.CatalystLiveTrigger = "TimeStep"
options.EnableCatalystLive = 1

producer = TrivialProducer(registrationName="steerchannel")


def catalyst_execute(info):
    global producer
    producer.UpdatePipeline()

    bounds = producer.GetDataInformation().GetBounds()

    # Detect MPI and rank based on bounds
    is_rank0 = bounds[0] == 0.0 and bounds[1] == 60.0
    is_rank1 = bounds[0] == 60.0 and bounds[1] == 120.0
    is_mpi = is_rank0 or is_rank1

    sliceInformation = producer.GetDataInformation()

    if not is_mpi:
        assert (
            sliceInformation.GetNumberOfCells() == 10320
        ), "Sliced data does not have the right number of cells"
        assert (
            sliceInformation.GetNumberOfPoints() == 5324
        ), "Sliced data does not have the right number of points"
    else:
        assert (
            sliceInformation.GetNumberOfCells() == 5160
        ), "Sliced data does not have the right number of cells"
        assert (
            sliceInformation.GetNumberOfPoints() == 2684
        ), "Sliced data does not have the right number of points"
