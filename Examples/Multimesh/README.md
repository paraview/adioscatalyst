# AdiosCatalyst example: multimesh

This example was mainly inspired by the `CxxFullExample` provided on ParaView [here](https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxFullExample).

This simulation follows the 'multimesh' protocol defined in [ParaView](https://kitware.github.io/paraview-docs/latest/cxx/ParaViewCatalystBlueprint.html). It's used to describe how a simulation of a multi block should be pass through catalyst api.

## Running the example

As described in the top-level README, this implementation is required to be launched with two separate command lines in the terminal, one for the simulation and another one for the visualization.

Set catalyst environment variable and launch the simulation executable:

```bash
export CATALYST_IMPLEMENTATION_PATHS="lib/catalyst"
bin/Example_Multimesh ../Examples/Commons/adios2.xml ../Examples/Multimesh/paraview_pipeline.py
```

On another terminal, start the visualization part:

```bash
export CATALYST_IMPLEMENTATION_PATHS="<paraview-build-location>/lib/catalyst"
export CATALYST_IMPLEMENTATION_NAME=paraview
bin/AdiosReplay ../Examples/Commons/adios2.xml
```

When running this simulation, the `paraview_pipeline.py` script will output the bounds of each grid inside the multiblock.

Note that if you want to specify a number of rank on the simulation part, you will need to update the `RendezvousReaderCount` and specify on it the number of rank in the [adios2.xml](adios2.xml).

## Using multiple readers

A second XML configuration file `adios2_2readers.xml`, provides an example of 2 visualization pipelines using the data from a single simulation. Note that the parameter `RendezvousReaderCount` has been set to 2: the simulation will wait for 2 ADIOS readers to connect before starting. `RendezvousReaderCount` does not depend on the number of MPI ranks of the simulation or readers. To run different visualization pipelines on both readers, the script can be specified as a command-line option for `AdiosReplay`. When `RendezvousReaderCount` is set to `0`, the simulation node will start without waiting for any readers. In this case, you have the possibility to set up an arbitrary number of readers to consume simulation data.

On one terminal, start the simulation:
```bash
export CATALYST_IMPLEMENTATION_PATHS="/lib/catalyst"
bin/Example_Multimesh ../Examples/Multimesh/testing/adios2_2readers.xml
```

On a second one, create a first visualization pipeline:
```bash
export CATALYST_IMPLEMENTATION_PATHS="<paraview-build-location>/lib/catalyst"
export CATALYST_IMPLEMENTATION_NAME=paraview
bin/AdiosReplay ../Examples/Multimesh/testing/adios2_2readers.xml ../Examples/Multimesh/paraview_pipeline.py
```

And on a third one, create a second visualization pipeline using a different script:
```bash
export CATALYST_IMPLEMENTATION_PATHS="<paraview-build-location>/lib/catalyst"
export CATALYST_IMPLEMENTATION_NAME=paraview
bin/AdiosReplay ../Examples/Multimesh/testing/adios2_2readers.xml ../Examples/Multimesh/paraview_pipeline_bis.py
```

In case both the simulation and the analysis node specify pipeline scripts, only those from the analysis node will be used.
