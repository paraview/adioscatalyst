#include "CatalystAdaptor.h"
#include "DataStructures.h"

#include <mpi.h>

#include <iostream>
#include <stdexcept>


/**
 * Example of a C++ adaptor for a simulation code where the simulation code has a fixed topology
 * grid. The output dataSet will be a multiblock composed of unstructured grid.
 *
 * This example was mostly inspired by the CxxFullExample from ParaView :
 * https://gitlab.kitware.com/paraview/paraview/-/blob/master/Examples/Catalyst2/CxxFullExample/FEDriver.cxx.
 *
 * It shows how multiple unstructured grid can be pass through the catalyst api as multi block.
 */
int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  // We create 2 Grids with differents abritrary parameters
  std::vector<Grid> grids;
  Grid grid1;
  unsigned int numPoints[3] = { 12, 3, 2 };
  double spacing[3] = { 1, 1.2, 2.9 };
  double position1[3] = { 0, 0, 0 };
  grid1.Initialize(numPoints, spacing, position1);
  grids.push_back(grid1);

  Grid grid2;
  double position2[3] = { 0, 3, 0 };
  grid2.Initialize(numPoints, spacing, position2);
  grids.push_back(grid2);

  std::vector<Attributes> attributes;
  Attributes attribute1;
  attribute1.Initialize(&grid1);
  attributes.push_back(attribute1);

  Attributes attribute2;
  attribute2.Initialize(&grid2);
  attributes.push_back(attribute2);

  CatalystAdaptor::Initialize(argc, argv);

  unsigned int numberOfTimeSteps = 10;
  for (unsigned int timeStep = 0; timeStep < numberOfTimeSteps; timeStep++)
  {
    // use a time step length of 0.1
    double time = timeStep * 0.1;
    for(std::size_t i = 0; i < attributes.size(); i++)
    {
      attributes[i].UpdateFields(time);
    }

    CatalystAdaptor::Execute(timeStep, time, grids, attributes);
  }

  CatalystAdaptor::Finalize();

  MPI_Finalize();

  return EXIT_SUCCESS;
}
