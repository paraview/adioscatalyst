from paraview.simple import *

print("executing paraview_pipeline")

producer = TrivialProducer(registrationName="input")

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'
options.EnableCatalystLive = 1

# Add here the pipeline code generated from paraview

# Special catalyst method called at each timestep, it is optional, it's use here
# only for print purpose
def catalyst_execute(info):
    global grid1, grid2
    producer.UpdatePipeline()

    grid1 = producer.GetSubsetDataInformation(0, "//grid1", "Hierarchy");
    grid2 = producer.GetSubsetDataInformation(0, "//grid2", "Hierarchy");

    print("-----------------------------------")
    print("executing (cycle={}, time={})".format(info.cycle, info.time))

    print("grid1:")
    print(" bounds:", grid1.GetBounds())

    print("grid2:")
    print(" bounds:", grid2.GetBounds())
