from paraview.simple import *

print("executing paraview_pipeline")

producer = TrivialProducer(registrationName="input")

# This script does a clip on the producer
clip1 = Clip(registrationName='Clip', Input=producer)
clip1.ClipType = 'Plane'
clip1.Scalars = ['CELLS', 'pressure']
clip1.Value = 1.0

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'
options.EnableCatalystLive = 1

print("Executing in another pipeline script.")

# Special catalyst method called at each timestep, it is optional, it's use here
# only for print purpose
def catalyst_execute(info):
    global grid1, grid2
    producer.UpdatePipeline()

    grid1 = producer.GetSubsetDataInformation(0, "//grid1", "Hierarchy");
    grid2 = producer.GetSubsetDataInformation(0, "//grid2", "Hierarchy");

    print("-----------------------------------")
    print("executing (cycle={}, time={})".format(info.cycle, info.time))

    print("grid1:")
    print(" bounds:", grid1.GetBounds())

    print("grid2:")
    print(" bounds:", grid2.GetBounds())
