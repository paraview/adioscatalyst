from paraview.simple import *

# Greeting to ensure that ctest knows this script is being imported
print("executing paraview_pipeline")

producer = TrivialProducer(registrationName="input")

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'

# ------------------------------------------------------------------------------
# List of expected values
expected_cycle = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
expected_bound = (0.0, 12.0, 0.0, 2.4, 0.0, 2.9)
expected_velocity_magnitude = [
    0.0,
    1.2000000000000002,
    2.4000000000000004,
    3.6000000000000005,
    4.800000000000001,
    6.0,
    7.200000000000001,
    8.4,
    9.600000000000001,
    10.8
]

expected_velocity_magnitude_rank0 = [
    0.0,
    0.6000000000000001,
    1.2000000000000002,
    1.8000000000000003,
    2.4000000000000004,
    3.0,
    3.6000000000000005,
    4.2,
    4.800000000000001,
    5.4
]

expected_pressure_magnitude = [
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0)
]

# ------------------------------------------------------------------------------
def test_with_mpi(info, producer, is_rank0):

    expected_bound_rank = [(0.0, 6.0, 0.0, 2.4, 0.0, 2.9), (6.0, 12.0, 0.0, 2.4, 0.0, 2.9)]

    if is_rank0:
        test_output_values(info, producer, expected_bound_rank[0], expected_velocity_magnitude_rank0)
    else:
        test_output_values(info, producer, expected_bound_rank[1], expected_velocity_magnitude)

# ------------------------------------------------------------------------------
def test_output_values(info,producer, expected_bound_rank, expected_velocity_magnitude_rank):
    # Test on grid1
    grid1 = producer.GetSubsetDataInformation(0, "//grid1", "Hierarchy");
    bounds = grid1.GetBounds()
    assert bounds == expected_bound_rank

    pressure = grid1.GetCellDataInformation().GetArrayInformation("pressure").GetComponentRange(0)
    assert pressure == expected_pressure_magnitude[info.cycle]

    velocity = grid1.GetPointDataInformation().GetArrayInformation("velocity").GetComponentRange(0)[1]
    assert velocity == expected_velocity_magnitude_rank[info.cycle], info.cycle.__str__() + " : " + velocity.__str__()

    # Test on grid2
    grid2 = producer.GetSubsetDataInformation(0, "//grid1", "Hierarchy");
    bounds2 = grid2.GetBounds()
    assert bounds2 == expected_bound_rank

    pressure2 = grid2.GetCellDataInformation().GetArrayInformation("pressure").GetComponentRange(0)
    assert pressure2 == expected_pressure_magnitude[info.cycle]

    velocity2 = grid2.GetPointDataInformation().GetArrayInformation("velocity").GetComponentRange(0)[1]
    assert velocity2 == expected_velocity_magnitude_rank[info.cycle]

# ------------------------------------------------------------------------------
def catalyst_execute(info):
    global grid1, grid2
    producer.UpdatePipeline()

    assert expected_cycle[info.cycle] == info.cycle, "'cycle' error, expected " + expected_cycle[info.cycle] + " but got " + info.cycle

    grid1 = producer.GetSubsetDataInformation(0, "//grid1", "Hierarchy");
    bounds = grid1.GetBounds()

    # On client side, their is not handy method to know the current rank, in this simple example the
    # rank0 will have bound in X axis between 0.0 and 6.0, and for rank 1 6.0 and 12.0
    is_rank0 = bounds[0] == 0.0 and bounds[1] == 6.0
    is_rank1 = bounds[0] == 6.0 and bounds[1] == 12.0
    is_mpi = is_rank0 or is_rank1

    if is_mpi:
        test_with_mpi(info, producer, is_rank0)
    else:
        test_output_values(info, producer, expected_bound, expected_velocity_magnitude)

