# AdiosCatalyst - Multi Writer : example of unstructured grid

## Introduction

### Context

In some scenario, a whole simulation can be composed of a subset of simulation which handle different usage. To be able to visualize it, we need to support to
connect several simulations to the visualization node.

### Solution

User will need to define a catalyst adaptor for each of theses simulations.
The custom catalyst2 implementation `AdiosCatalyst` will create an Adios2 context for each of them.
Then, the `AdiosReplay` will be responsible for aggregating each entry into a single conduit node for the
`ParaViewCatalyst` implementation as described in the following schema:

![multiple_simulation_scenario](data/img/schema.png)

## About this example

This example was mainly inspired by the `CxxFullExample` provided by ParaView [here](https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxFullExample).


## Build and dependency

All dependencies are described in the main [README](../../README.md) of this repository.

## How run the example

As described in the main README, this implementation required to be launch with **three** separate command lines in the terminal, **two** for the simulation and another one for the visualization.

To start the simulation with this example set catalyst environment variable and launch the executable:

For the first simulation:

```bash
export CATALYST_IMPLEMENTATION_PATHS="/lib/catalyst"
bin/Example_Multiple_Writer_Simu1 ../Examples/MultipleWriter/adios2.xml ../Examples/MultipleWriter/paraview_pipeline.py
```

For the second one:

```bash
export CATALYST_IMPLEMENTATION_PATHS="/lib/catalyst"
bin/Example_Multiple_Writer_Simu2 ../Examples/MultipleWriter/adios2.xml ../Examples/MultipleWriter/paraview_pipeline.py
```

Last, on another terminal to start the visualization part run the above command:

```bash
export CATALYST_IMPLEMENTATION_PATHS="<paraview-build-location>/lib/catalyst"
export CATALYST_IMPLEMENTATION_NAME=paraview
bin/AdiosReplay ../Examples/MultipleWriter/adios2.xml gs1.bp gs2.bp
```

These 2 simulations creates a dummy unstructured grid which are append into a multimesh by the AdiosReplay.
