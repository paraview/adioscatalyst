from paraview.simple import *
import time

print("executing paraview_pipeline")

producer = TrivialProducer(registrationName="input")

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'
options.EnableCatalystLive = 1

# ------------------------------------------------------------------------------
# Add here the pipeline code generated from paraview

# ------------------------------------------------------------------------------
# Special catalyst method called at each timestep, it is optional, it's use here
# only for print purpose
def catalyst_execute(info):
    global producer
    producer.UpdatePipeline()
    print("-----------------------------------")
    print("executing (cycle={}, time={}).".format(info.cycle, info.time))

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    from paraview.simple import SaveExtractsUsingCatalystOptions
    # Code for non in-situ environments; if executing in post-processing
    # i.e. non-Catalyst mode, let's generate extracts using Catalyst options
    SaveExtractsUsingCatalystOptions(options)
