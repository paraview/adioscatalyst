#include "CatalystAdaptor.h"
#include "../Commons/DataStructures.h"

#include <mpi.h>

#include <iostream>
#include <stdexcept>

#include <unistd.h> // for std::sleep

int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  Grid grid;
  unsigned int numPoints[3] = { 10, 60, 44 };
  double spacing[3] = { 1, 1.2, 2.9 };
  double position[3] = { 0,0,0 };
  grid.Initialize(numPoints, spacing, position);
  Attributes attributes;
  attributes.Initialize(&grid);
  attributes.SetSSTFileName("gs1.bp");

  CatalystAdaptor::Initialize(argc, argv);
  unsigned int numberOfTimeSteps = 10;
  for (unsigned int timeStep = 0; timeStep < numberOfTimeSteps; timeStep++)
  {
    // Force to async simu1 and simu2 manually
    sleep(1);

    // use a time step length of 0.1
    double time = timeStep * 0.1;
    attributes.UpdateFields(time);
    CatalystAdaptor::Execute(timeStep, time, grid, attributes);
  }

  CatalystAdaptor::Finalize();

  MPI_Finalize();

  return EXIT_SUCCESS;
}
