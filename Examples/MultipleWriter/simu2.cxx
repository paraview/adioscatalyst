#include "CatalystAdaptor.h"
#include "../Commons/DataStructures.h"

#include <mpi.h>

#include <iostream>
#include <stdexcept>

int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  Grid grid;

  // Offset to be relative to the first one : check the simu1.cxx
  unsigned int numPoints[3] = { 110, 60, 44 };
  double spacing[3] = { 1, 1.2, 2.9 };
  double position[3] = { 100,0,0 };
  
  grid.Initialize(numPoints, spacing, position);
  Attributes attributes;
  attributes.Initialize(&grid);
  attributes.SetSSTFileName("gs2.bp");

  CatalystAdaptor::Initialize(argc, argv);
  unsigned int numberOfTimeSteps = 10;
  for (unsigned int timeStep = 0; timeStep < numberOfTimeSteps; timeStep++)
  {
    // use a time step length of 0.1
    double time = timeStep * 0.1;
    attributes.UpdateFields(time);
    CatalystAdaptor::Execute(timeStep, time, grid, attributes);
  }

  CatalystAdaptor::Finalize();

  MPI_Finalize();

  return EXIT_SUCCESS;
}
