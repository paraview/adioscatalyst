from paraview import catalyst
from paraview.simple import *

# Greeting to ensure that ctest knows this script is being imported
print("executing paraview_pipeline")

producer = TrivialProducer(registrationName="input")

# ------------------------------------------------------------------------------
# Catalyst options
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'

# ------------------------------------------------------------------------------
# List of expected values
expected_cycle = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
expected_bound = (0.0, 10.0, 0.0, 70.8, 0.0, 124.7)
expected_bound2 = (100.0, 210.0, 0.0, 70.8, 0.0, 124.7)
expected_velocity1_magnitude = [
    0.0,
    1.0,
    2.0,
    3.0000000000000004,
    4.0,
    5.0,
    6.000000000000001,
    7.000000000000001,
    8.0,
    9.0,
    10.0
]

expected_velocity2_magnitude = [
    0.0,
    21.0,
    42.0,
    63.00000000000001,
    84.0,
    105.0,
    126.00000000000001,
    147.0,
    168.0,
    189.0,
    220.0
]

expected_pressure_magnitude = [
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0)
]

# ------------------------------------------------------------------------------
def test_with_mpi(info, producer, is_rank0):

    expected_bound_rank = [
        (0.0, 5.0, 0.0, 70.8, 0.0, 124.7),
        (5.0, 10.0, 0.0, 70.8, 0.0, 124.7),
        (10.0, 55.0, 0.0, 70.8, 0.0, 124.7),
        (55.0, 100.0, 0.0, 70.8, 0.0, 124.7)]

    if is_rank0:
        test_output_values(
            info, producer, expected_bound_rank[0], expected_bound_rank[1], expected_velocity1_magnitude, expected_velocity2_magnitude)
    else:
        test_output_values(
            info, producer, expected_bound_rank[2], expected_bound_rank[3], expected_velocity1_magnitude, expected_velocity2_magnitude)

# ------------------------------------------------------------------------------
def test_output_values(info, producer, expected_bound_rank, expected_bound_rank2, expected_velocity1, expected_velocity2):
    # Test on grid1
    grid1 = producer.GetSubsetDataInformation(0, "//simulation0", "Hierarchy")
    bounds = grid1.GetBounds()
    assert bounds == expected_bound_rank, "print bounds : " + bounds.__str__()

    pressure = grid1.GetCellDataInformation().GetArrayInformation(
        "pressure").GetComponentRange(0)
    assert pressure == expected_pressure_magnitude[info.cycle], "\nprint :" + \
        pressure.__str__()

    velocity = grid1.GetPointDataInformation().GetArrayInformation(
        "velocity").GetComponentRange(0)[1]
    assert velocity == expected_velocity1[info.cycle], info.cycle.__str__(
    ) + " : \n" + velocity.__str__()

    # Test on grid2
    grid2 = producer.GetSubsetDataInformation(0, "//simulation1", "Hierarchy")
    bounds2 = grid2.GetBounds()
    assert bounds2 == expected_bound_rank2, "print bounds : " + bounds2.__str__()

    pressure2 = grid2.GetCellDataInformation().GetArrayInformation(
        "pressure").GetComponentRange(0)
    assert pressure2 == expected_pressure_magnitude[info.cycle]

    velocity2 = grid2.GetPointDataInformation().GetArrayInformation(
        "velocity").GetComponentRange(0)[1]
    assert velocity2 == expected_velocity2[info.cycle], info.cycle.__str__(
    ) + " : \n" + velocity2.__str__()

# ------------------------------------------------------------------------------
def catalyst_execute(info):
    global grid1, grid2
    producer.UpdatePipeline()

    assert expected_cycle[info.cycle] == info.cycle, "'cycle' error, expected " + \
        expected_cycle[info.cycle] + " but got " + info.cycle

    grid1 = producer.GetSubsetDataInformation(0, "//simulation0", "Hierarchy")
    bounds = grid1.GetBounds()
    print(bounds.__str__())

    # On client side, their is not handy method to know the current rank, in this simple example the
    # rank0 will have bound in X axis between 0.0 and 6.0, and for rank 1 6.0 and 12.0
    is_rank0 = bounds[0] == 0.0 and bounds[1] == 5.0
    is_rank1 = bounds[0] == 5.0 and bounds[1] == 10.0
    is_mpi = is_rank0 or is_rank1

    if is_mpi:
        test_with_mpi(info, producer, is_rank0)
    else:
        test_output_values(info, producer, expected_bound, expected_bound2,
                           expected_velocity1_magnitude, expected_velocity2_magnitude)
