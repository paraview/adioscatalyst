#!/usr/bin/env python
import subprocess
from subprocess import PIPE
import sys
import os

os.chdir(os.path.dirname(__file__))

# Retrieve simulation executables
build_folder = sys.argv[1]
simulation1_executable = build_folder + "/bin/Example_Multi_Writer_Simu1"
simulation2_executable = build_folder + "/bin/Example_Multi_Writer_Simu2"
visualization_executable = build_folder + "/bin/AdiosReplay"

simulation1_command = ""
simulation2_command = ""
visualization_command = ""

# Add the MPI prefix if MPI is active
if len(sys.argv) == 5:
    simulation1_command += " mpirun -np 2"
    simulation2_command += " mpirun -np 2"
    visualization_command += " mpirun -np 2"

simulation1_command += (
    f" {simulation1_executable} ../../MultipleWriter/adios2.xml"
)
simulation2_command += (
    f" {simulation2_executable} ../../MultipleWriter/adios2.xml"
)

visualization_command = f" {visualization_executable} ../../MultipleWriter/adios2.xml gs1.bp gs2.bp paraview_pipeline.py"

simulation_env = os.environ.copy()
simulation_env["CATALYST_IMPLEMENTATION_PATHS"] = sys.argv[2]
visualization_env = os.environ.copy()
visualization_env["CATALYST_IMPLEMENTATION_PATHS"] = sys.argv[3]

simulation1_process = subprocess.Popen(
    simulation1_command, stdout=PIPE, stderr=PIPE, shell=True, env=simulation_env
)

simulation2_process = subprocess.Popen(
    simulation2_command, stdout=PIPE, stderr=PIPE, shell=True, env=simulation_env
)

visualization_process = subprocess.Popen(
    visualization_command, stdout=PIPE, stderr=PIPE, shell=True, env=visualization_env
)

stdout1, stderr1 = simulation1_process.communicate()
stdout2, stderr2 = simulation2_process.communicate()
stdout3, stderr3 = visualization_process.communicate()

assert stderr1 == b"", f"Error on the first simulation side: {stderr1}"
assert stderr2 == b"", f"Error on the second simulation side: {stderr2}"
assert stderr3 == b"", f"Error on visualization side: {stderr3}"
