#ifndef CatalystAdaptor_h
#define CatalystAdaptor_h

#include "../Commons/DataStructures.h"
#include <catalyst.hpp>

#include <array>
#include <iostream>
#include <string>

namespace CatalystAdaptor
{
void Initialize(int argc, char* argv[])
{
  conduit_cpp::Node node;
  for (int cc = 1; cc < argc; ++cc)
  {
    if (strstr(argv[cc], "xml"))
    {
      node["adios/config_filepath"].set_string(argv[cc]);
    }
    else
    {
      node["catalyst/scripts/script" + std::to_string(cc - 1)].set_string(argv[cc]);
    }
  }
  node["catalyst_load/implementation"] = "adios";
  catalyst_status err = catalyst_initialize(conduit_cpp::c_node(&node));
  if (err != catalyst_status_ok)
  {
    std::cerr << "Failed to initialize Catalyst: " << err << std::endl;
  }
}

void Execute(int cycle, double time, Grid& grid, Attributes& attribs)
{
  conduit_cpp::Node exec_params;

  // add time/cycle information
  auto state = exec_params["catalyst/state"];
  state["timestep"].set(cycle);
  state["time"].set(time);
  state["multiblock"].set(1);

  // Add channels.
  // We only have 1 channel here. Let's name it 'grid'.
  auto channel = exec_params["catalyst/channels/grid"];

  // Since this example is using Conduit Mesh Blueprint to define the mesh,
  // we set the channel's type to "mesh".
  channel["type"].set("mesh");

  // now create the mesh.
  auto mesh = channel["data"];

  // start with coordsets (of course, the sequence is not important, just make
  // it easier to think in this order).
  mesh["coordsets/coords/type"].set("explicit");

  mesh["coordsets/coords/points_shape"].set_uint64(grid.GetPointsShape());
  mesh["coordsets/coords/points_start"].set_uint64(grid.GetPointsStart());
  mesh["coordsets/coords/points_count"].set_uint64(grid.GetPointsCount());

  mesh["coordsets/coords/values/x"].set_external(
    grid.GetPointsArray(), grid.GetNumberOfPoints(), /*offset=*/0, /*stride=*/3 * sizeof(double));
  mesh["coordsets/coords/values/y"].set_external(grid.GetPointsArray(), grid.GetNumberOfPoints(),
    /*offset=*/sizeof(double), /*stride=*/3 * sizeof(double));
  mesh["coordsets/coords/values/z"].set_external(grid.GetPointsArray(), grid.GetNumberOfPoints(),
    /*offset=*/2 * sizeof(double), /*stride=*/3 * sizeof(double));

  // Next, add topology
  mesh["topologies/mesh/type"].set("unstructured");
  mesh["topologies/mesh/coordset"].set("coords");
  mesh["topologies/mesh/cells_shape"].set_uint64(grid.GetCellsShape());
  mesh["topologies/mesh/cells_start"].set_uint64(grid.GetCellsStart());
  mesh["topologies/mesh/cells_count"].set_uint64(grid.GetCellsCount());
  mesh["topologies/mesh/elements/shape"].set("hex");
  mesh["topologies/mesh/elements/connectivity"].set_external(
    grid.GetCellPoints(0), grid.GetNumberOfCells() * 8);

  // Finally, add fields.
  auto fields = mesh["fields"];
  fields["velocity/association"].set("vertex");
  fields["velocity/topology"].set("mesh");
  fields["velocity/volume_dependent"].set("false");

  // velocity is stored in non-interlaced form (unlike points).
  fields["velocity/values/x"].set_external(
    attribs.GetVelocityArray(), grid.GetNumberOfPoints(), /*offset=*/0);
  fields["velocity/values/y"].set_external(attribs.GetVelocityArray(), grid.GetNumberOfPoints(),
    /*offset=*/grid.GetNumberOfPoints() * sizeof(double));
  fields["velocity/values/z"].set_external(attribs.GetVelocityArray(), grid.GetNumberOfPoints(),
    /*offset=*/grid.GetNumberOfPoints() * sizeof(double) * 2);

  // pressure is cell-data.
  fields["pressure/association"].set("element");
  fields["pressure/topology"].set("mesh");
  fields["pressure/volume_dependent"].set("false");
  fields["pressure/values"].set_external(attribs.GetPressureArray(), grid.GetNumberOfCells());

  // char cell-data.
  static std::vector<signed char> char_field(grid.GetNumberOfCells(), -27);
  fields["char-field/association"].set("element");
  fields["char-field/topology"].set("mesh");
  fields["char-field/volume_dependent"].set("false");
  fields["char-field/values"].set_external(char_field.data(), char_field.size());

  // unsigned char cell-data.
  static std::vector<unsigned char> unsigned_char_field(grid.GetNumberOfCells(), 240);
  fields["uchar-field/association"].set("element");
  fields["uchar-field/topology"].set("mesh");
  fields["uchar-field/volume_dependent"].set("false");
  fields["uchar-field/values"].set_external(
    unsigned_char_field.data(), unsigned_char_field.size());

  catalyst_status err = catalyst_execute(conduit_cpp::c_node(&exec_params));
  if (err != catalyst_status_ok)
  {
    std::cerr << "Failed to execute Catalyst: " << err << std::endl;
  }
}

void Finalize()
{
  conduit_cpp::Node node;
  catalyst_status err = catalyst_finalize(conduit_cpp::c_node(&node));
  if (err != catalyst_status_ok)
  {
    std::cerr << "Failed to finalize Catalyst: " << err << std::endl;
  }
}
}

#endif
