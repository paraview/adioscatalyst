# AdiosCatalyst example: unstructured grid

This example was mainly inspired by the Catalyst2 `CxxFullExample` provided in ParaView [here](https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxFullExample).

## Running the example

As described in the top-level README, this implementation is required to be launched with two separate command lines in the terminal, one for the simulation and another one for the visualization.

From the build directory, set Catalyst path environment variable and launch the simulation executable:

```bash
export CATALYST_IMPLEMENTATION_PATHS="lib/catalyst"
bin/Example_UnstructuredGrid ../Examples/Commons/adios2.xml ../Examples/UnstructuredGrid/paraview_pipeline.py
```

On another terminal, start the visualization part:

```bash
export CATALYST_IMPLEMENTATION_PATHS="<paraview-build-location>/lib/catalyst"
export CATALYST_IMPLEMENTATION_NAME=paraview
bin/AdiosReplay ../Examples/Commons/adios2.xml
```

On the visualization side, when the simulation running, the script `catalyst_pipeline.py` will output the bounds of each field for each step, e.g. for timestep 0:

```
executing paraview_pipeline
-----------------------------------
executing (cycle=0, time=0.0)
bounds: (0.0, 10.0, 0.0, 2.4, 0.0, 5.8)
velocity-magnitude-range: (0.0, 14.142135623730951)
pressure-range: (1.0, 1.0)
```
