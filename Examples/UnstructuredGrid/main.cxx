#include "CatalystAdaptor.h"
#include "../Commons/DataStructures.h"

#include <mpi.h>

#include <iostream>
#include <stdexcept>

/**
 * Example of a C++ adaptor for a simulation code where the simulation code has a fixed topology
 * grid.
 *
 * This example was mostly inspired by the CxxFullExample from ParaView :
 * https://gitlab.kitware.com/paraview/paraview/-/blob/master/Examples/Catalyst2/CxxFullExample/FEDriver.cxx
 *
 * This grid is an unstructured grid with cell data (pressure) and field data (velocity).
 * Note that only the velocity data will be updated at each timestep for testing.
 */
int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  Grid grid;
  unsigned int numPoints[3] = { 10, 3, 3 };
  double spacing[3] = { 1, 1.2, 2.9 };
  double position[3] = { 0,0,0 };
  grid.Initialize(numPoints, spacing, position);
  Attributes attributes;
  attributes.Initialize(&grid);
  attributes.SetSSTFileName("gs.bp");

  CatalystAdaptor::Initialize(argc, argv);
  unsigned int numberOfTimeSteps = 10;
  for (unsigned int timeStep = 0; timeStep < numberOfTimeSteps; timeStep++)
  {
    // use a time step length of 0.1
    double time = timeStep * 0.1;
    attributes.UpdateFields(time);
    CatalystAdaptor::Execute(timeStep, time, grid, attributes);
  }

  CatalystAdaptor::Finalize();

  MPI_Finalize();

  return EXIT_SUCCESS;
}
