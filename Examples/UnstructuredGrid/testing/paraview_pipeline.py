from paraview.simple import *

print("executing paraview_pipeline")

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'

# registrationName must match the channel name used in the
# 'CatalystAdaptor'.
producer = TrivialProducer(registrationName="grid")


# ------------------------------------------------------------------------------
# List of expected values
expected_cycle = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
expected_bound = (0.0, 10.0, 0.0, 2.4, 0.0, 5.8)
expected_velocity_magnitude = [
    14.142135623730951,
    14.757845371191555,
    15.439430041293624,
    16.17856606748571,
    16.967734085610843,
    17.80028089666003,
    18.670404387693377,
    19.573093776917332,
    20.504048380746667,
    21.459589930844437
]

expected_velocity_magnitude_rank0 = [
    7.0710678118654755,
    7.670984291471337,
    8.305179107039173,
    8.966381655941264,
    9.649041403165395,
    10.348912986396204,
    11.062730223593089,
    11.787959959212621,
    12.522619534266783,
    13.26514229098203
]

expected_pressure_magnitude = [
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0),
    (1.0, 1.0)
]

expected_char_range = (-27.0, -27.0)
expected_uchar_range = (240.0, 240.0)

# ------------------------------------------------------------------------------
def test_with_mpi(info, producer, is_rank0):

    expected_bound_rank = [(0.0, 5.0, 0.0, 2.4, 0.0, 5.8), (5.0, 10.0, 0.0, 2.4, 0.0, 5.8)]

    if is_rank0:
        test_output_values(info, producer, expected_bound_rank[0], expected_velocity_magnitude_rank0)
    else:
        test_output_values(info, producer, expected_bound_rank[1], expected_velocity_magnitude)

# ------------------------------------------------------------------------------
def test_output_values(info,producer, expected_bound_rank, expected_velocity_magnitude_rank):
    bounds = producer.GetDataInformation().GetBounds()
    assert bounds == expected_bound_rank, "expected bound :" + expected_bound_rank.__str__() + " but got " + bounds.__str__()

    velocity_magnitude = producer.PointData["velocity"].GetRange(-1)[1]
    expected_velocity = expected_velocity_magnitude_rank[info.cycle]
    assert velocity_magnitude == expected_velocity
    
    expected_pressure = expected_pressure_magnitude[info.cycle]
    pressure_range = producer.CellData["pressure"].GetRange(0)
    assert pressure_range == expected_pressure, "expected pressure :" + expected_pressure_magnitude.__str__() + " but got " + pressure_range.__str__()
    
    char_range = producer.CellData["char-field"].GetRange(0)
    assert char_range == expected_char_range, "expected char range :" + expected_char_range.__str__() + " but got " + char_range.__str__()

    uchar_range = producer.CellData["uchar-field"].GetRange(0)
    assert uchar_range == expected_uchar_range, "expected uchar range :" + expected_uchar_range.__str__() + " but got " + uchar_range.__str__()

# ------------------------------------------------------------------------------
def catalyst_execute(info):
    global producer

    assert expected_cycle[info.cycle] == info.cycle, "'cycle' error, expected " + expected_cycle[info.cycle] + " but got " + info.cycle

    producer.UpdatePipeline()
    bounds = producer.GetDataInformation().GetBounds()

    # On client side, their is not handy method to know the current rank, in this simple example the
    # rank0 will have bound in X axis between 0.0 and 5.0, and for rank 1 5.0 and 10.0
    is_rank0 = bounds[0] == 0.0 and bounds[1] == 5.0
    is_rank1 = bounds[0] == 5.0 and bounds[1] == 10.0
    is_mpi = is_rank0 or is_rank1

    if is_mpi:
        test_with_mpi(info, producer, is_rank0)
    else:
        test_output_values(info, producer, expected_bound, expected_velocity_magnitude)

