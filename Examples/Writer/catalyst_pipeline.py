from paraview.simple import *

# Greeting to ensure that ctest knows this script is being imported
print("executing paraview_pipeline")

producer = TrivialProducer(registrationName="grid")

from paraview import catalyst

options = catalyst.Options()

def catalyst_execute(info):
    global producer
    producer.UpdatePipeline()
    print("-----------------------------------")
    print("executing (cycle={}, time={})".format(info.cycle, info.time))
    print("bounds:", producer.GetDataInformation().GetBounds())
    print("velocity-magnitude-range:", producer.PointData["velocity"].GetRange(-1))
    print("pressure-range:", producer.CellData["pressure"].GetRange(0))
