#include "../Commons/DataStructures.h"

#include <mpi.h>

#include "CatalystAdaptor.h"

#include <iostream>
#include <stdexcept>

// Test that the writer part can run, instanciate adios property and finish without failing
int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  Grid grid;
  unsigned int numPoints[3] = { 5, 2, 2 };
  double spacing[3] = { 1, 1.2, 2.9 };
  double position[3] = {0.0, 0.0, 0.0};
  grid.Initialize(numPoints, spacing, position);
  Attributes attributes;
  attributes.Initialize(&grid);

  CatalystAdaptor::Initialize(argc, argv);
  unsigned int numberOfTimeSteps = 3;
  for (unsigned int timeStep = 0; timeStep < numberOfTimeSteps; timeStep++)
  {
    double time = timeStep * 0.1;
    attributes.UpdateFields(time);
    CatalystAdaptor::Execute(timeStep, time, grid, attributes);
  }

  CatalystAdaptor::Finalize();

  MPI_Finalize();

  return EXIT_SUCCESS;
}
