# AdiosCatalyst

AdiosCatalyst is a new [Catalyst](https://gitlab.kitware.com/paraview/catalyst) implementation for [Adios2](https://github.com/ornladios/ADIOS2). The aim of this specific implementation is to be able to make in-transit simulation with the [SST](https://adios2.readthedocs.io/en/latest/engines/engines.html#sst-sustainable-staging-transport) engine of ADIOS2.


For more detail, the initial design of this implementation of Catalyst2 with Adios2 has been described [here](https://discourse.paraview.org/t/in-transit-catalyst-implementation-based-on-adios2/9718).

## Requirements

AdiosCatalyst depends on:
- [ADIOS2](https://adios2.readthedocs.io/en/latest/) : version 2.8 or newer.
- [Catalyst2](https://catalyst-in-situ.readthedocs.io/en/latest/) : version v2.0.0-rc4 or newer.

Set `ADIOS2_DIR` and `catalyst_DIR` CMake variables to the directories of those projects.

Note that you also need `MPI` because it is an ADIOS2 requirement.

## Getting started

A full in-transit visualization pipeline using AdiosCatalyst runs in two separate executables. The simulation node uses the `AdiosCatalyst` Catalyst implementation, which converts the conduit node created in your simulation to Adios2. The analyis node (named `AdiosReplay`) does the opposite operation, converting Adios2 data back to a Conduit node, and delegates the visualization to the `ParaviewCatalyst` Catalyst implementation.

The separation of the simulation node and the analysis node allows "In-transit" visualization pipelines, where the visualization and the simulation can be done asynchronously, as opposed to "In-situ" techniques, thanks to ADIOS2 and MPI.

### Simulation Nodes

The simulation node needs to use the AdiosCatalyst Catalyst2 implementation. For this, set the environment variables for Catalyst2:

```bash
export CATALYST_IMPLEMENTATION_PATHS="<AdiosCatalyst-build-location>/lib/catalyst" \
export CATALYST_IMPLEMENTATION_NAME=adios
```

Then, start your simulation. Multiple examples of simulations along with instruction to run them can be found in the `Examples` directory.

### Analysis Nodes

This part implements an ADOIS2 reader to read ADIOS2 data and recreates conduit node for Catalyst2, as output we trigger another catalyst implementation for the visualization.
Currently we use the ParaView implementation of Catalyst2 for that.

Specify the environment variable `CATALYST_IMPLEMENTATION_PATHS` for Catalyst2 (`CATALYST_IMPLEMENTATION_NAME` is automatically set to `paraview`), and start the visualization node :

```bash
export CATALYST_IMPLEMENTATION_PATHS="<ParaViewCatalyst-build-location>/lib/catalyst"
bin/AdiosReplay <adios-xml-config-file> [<catalyst-script-file>]
```

The ADIOS2 XML configuration file must be the same that the one provided to AdiosCatalyst by the simulation. The Catalyst Python script file can be either provided by the simulation to AdiosCatalyst or in the AdiosReplay command-line arguments.

## Reader multiplexing

Multiple `AdiosReplay` processes can be launched with different simulation pipelines using a single simulation executable. An example of this is presented in `Examples/Multimesh`.

## Hybrid in-transit/in-situ pipeline

For heavier datasets, an hybrid in-transit/in-situ mode can be used. The data is first reduced by an in-situ pipeline before being sent to the visualisation node through ADIOS2. Read more about this in `Examples/Hybrid`.

## Testing

Set the CMake variable `BUILD_TESTING` to `ON`, configure and build. Then run `ctest`. Every example is tested end-to-end both with and without MPI.

## Limitations

- The main difference between Catalyst2 and ADIOS2 is the `Initialize` method. For adios2, to define a variable we need to know its dimensions, but we don't have access to the data yet during the Catalyst `Initialize` step. As a consequence, we define these variables during the first `Execute` call.

- The current implementation support only `explicit` [mesh blueprints](https://llnl-conduit.readthedocs.io/en/latest/blueprint_mesh.html), such as `unstructured` topologies.

- Only Linux is supported for now.

## Citation

To cite this work, please use the following:

Mazen, F., Givord, L., Gueunet, C. (2023). Catalyst-ADIOS2: In Transit Analysis for Numerical Simulations Using Catalyst 2 API. In: Bienz, A., Weiland, M., Baboulin, M., Kruse, C. (eds) High Performance Computing. ISC High Performance 2023. Lecture Notes in Computer Science, vol 13999. Springer, Cham. https://doi.org/10.1007/978-3-031-40843-4_20

## License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
